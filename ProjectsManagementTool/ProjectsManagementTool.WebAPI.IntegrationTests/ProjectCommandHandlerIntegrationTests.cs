﻿using CollectionsAndLinq.Services;
using ProjectsManagementTool.Common.DTOs;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace ProjectsManagementTool.WebAPI.IntegrationTests
{
    public class ProjectCommandHandlerIntegrationTests : BaseIntegrationTests
    {
        private const string _baseUrlChunck = "api/projects";

        public ProjectCommandHandlerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
            : base(factory) { }

        [Fact]
        public async Task AddProject_ThanResponseCode201AndCorrectResponseBody()
        {
            const int projectId = 1;
            var project = new ProjectDto { Id = projectId, Name = "Name111", Description = "Description111", AuthorId = null };

            var projectResult = await HttpClientWrapper.PostAsync<ProjectDto>(_baseUrlChunck, Client, project);

            await Client.DeleteAsync($"{_baseUrlChunck}/{projectId}");

            Assert.Equal(HttpStatusCode.Created, projectResult.Response.StatusCode);
            Assert.Equal(projectId, projectResult.Result.Id);
        }

        [Fact]
        public async Task AddProjectWithExistingId_ThanResponseCode409AndCorrectResponseBody()
        {
            const int projectId = 1;
            var project1 = new ProjectDto { Id = projectId, Name = "Name111" };
            var project2 = new ProjectDto { Id = projectId, Name = "Name111" };

            var resultOne = await HttpClientWrapper.PostAsync<ProjectDto>(
                _baseUrlChunck, Client, project1);

            var resultTwo = await HttpClientWrapper.PostAsync<ProjectDto>(
                _baseUrlChunck, Client, project2);

            await Client.DeleteAsync($"{_baseUrlChunck}/{projectId}");

            Assert.Equal(HttpStatusCode.Created, resultOne.Response.StatusCode);
            Assert.Equal(HttpStatusCode.Conflict, resultTwo.Response.StatusCode);
            Assert.Equal(projectId, resultOne.Result.Id);
            Assert.Equal(project1.Name, resultOne.Result.Name);
        }
    }
}
