﻿using CollectionsAndLinq.Services;
using ProjectsManagementTool.Common.DTOs;
using ProjectsManagementTool.Common.Models;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace ProjectsManagementTool.WebAPI.IntegrationTests
{
    public class TaskCommandHandlerIntegrationTests : BaseIntegrationTests
    {
        private const string _baseUrlChunck = "api/tasks";

        public TaskCommandHandlerIntegrationTests(CustomWebApplicationFactory<Startup> factory) : base(factory) { }

        [Fact]
        public async Task DeleteTask_ThanResponseCode204AndCorrectResponseBody()
        {
            const int taskId = 1;
            var task = new TaskDto { Id = taskId, Name = "Name111", Description = "Description", State = TaskState.ToDo };

            var taskResult = await HttpClientWrapper.PostAsync<TaskDto>(_baseUrlChunck, Client, task);

            var deleteTaskResult = await HttpClientWrapper.DeleteAsync(_baseUrlChunck, taskId, Client);

            var checkResult = await HttpClientWrapper.GetAsync<TaskDto>(_baseUrlChunck, taskId, Client);

            Assert.Equal(HttpStatusCode.Created, taskResult.Response.StatusCode);
            Assert.Equal(HttpStatusCode.NoContent, deleteTaskResult.StatusCode);
            Assert.Equal(HttpStatusCode.NotFound, checkResult.Response.StatusCode);
            Assert.Equal(taskId, taskResult.Result.Id);
        }

        [Fact]
        public async Task DeleteNotExistingTask_ThanResponseCode404()
        {
            const int taskId = 1;

            var deletedResponse = await HttpClientWrapper.DeleteAsync(_baseUrlChunck, taskId, Client);

            Assert.Equal(HttpStatusCode.NotFound, deletedResponse.StatusCode);
        }
    }
}
