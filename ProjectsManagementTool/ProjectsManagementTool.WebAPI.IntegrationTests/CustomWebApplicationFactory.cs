﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using ProjectsManagementTool.DAL.Context;
using System;
using System.Linq;

namespace ProjectsManagementTool.WebAPI.IntegrationTests
{
    public class CustomWebApplicationFactory<TStartup> : WebApplicationFactory<Startup> where TStartup : class
    {
        public IServiceScope Scope { get; }
        protected PmtContext Context { get; }
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.ConfigureServices(services =>
            {
                // Remove the app's ApplicationDbContext registration.
                var descriptor = services.SingleOrDefault(
                    d => d.ServiceType ==
                        typeof(DbContextOptions<PmtContext>));

                if (descriptor != null)
                {
                    services.Remove(descriptor);
                }

                // Add ApplicationDbContext using an in-memory database for testing.
                services.AddDbContext<PmtContext>(options =>
                {
                    options.UseInMemoryDatabase($"InMemoryDbForTesting{new Guid()}");
                    options.EnableServiceProviderCaching(true);
                    options.EnableDetailedErrors(true);
                });

                // Build the service provider.
                var sp = services.BuildServiceProvider();

                PmtContext.ShouldDbBeSeeded = false;
            });
        }
    }
}
