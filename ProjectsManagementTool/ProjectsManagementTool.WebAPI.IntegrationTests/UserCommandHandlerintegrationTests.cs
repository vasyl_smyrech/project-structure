﻿using CollectionsAndLinq.Services;
using ProjectsManagementTool.Common.DTOs;
using System;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace ProjectsManagementTool.WebAPI.IntegrationTests
{
    public class UserCommandHandlerintegrationTests : BaseIntegrationTests
    {
        private const string _baseUrlChunck = "api/users";

        public UserCommandHandlerintegrationTests(CustomWebApplicationFactory<Startup> factory) : base(factory) { }

        [Fact]
        public async Task AddUser_ThanResponseCode201AndCorrectResponseBody()
        {
            const int userId = 1;
            var user = new UserDto { Id = userId, FirstName = "FirstName111", LastName = "LastName1", Email = "test@test.com", Birthday = DateTime.Now.AddYears(-40) };

            var userResponse = await HttpClientWrapper.PostAsync<UserDto>(_baseUrlChunck, Client, user);

            await HttpClientWrapper.DeleteAsync(_baseUrlChunck, userId, Client);

            Assert.Equal(HttpStatusCode.Created, userResponse.Response.StatusCode);
            Assert.Equal(userId, userResponse.Result.Id);
        }

        [Fact]
        public async Task AddUserWithExistingId_ThanResponseCode409AndCorrectResponseBody()
        {
            const int userId = 1;
            var user1 = new UserDto { Id = userId, FirstName = "FirstName111", LastName = "LastName1", Email = "test@test.com", Birthday = DateTime.Now.AddYears(-30) };
            var user2 = new UserDto { Id = userId, FirstName = "FirstName222", LastName = "LastName2", Email = "test@test.com", Birthday = DateTime.Now.AddYears(-30) };

            var user1Result = await HttpClientWrapper.PostAsync<UserDto>(_baseUrlChunck, Client, user1);

            var user2Result = await HttpClientWrapper.PostAsync<UserDto>(_baseUrlChunck, Client, user2);

            await HttpClientWrapper.DeleteAsync(_baseUrlChunck, userId, Client);

            Assert.Equal(HttpStatusCode.Created, user1Result.Response.StatusCode);
            Assert.Equal(HttpStatusCode.Conflict, user2Result.Response.StatusCode);
            Assert.Equal(userId, user1Result.Result.Id);
            Assert.Equal(user1.FirstName, user1Result.Result.FirstName);
        }

        [Fact]
        public async Task DeleteUser_ThanResponseCode204AndCorrectResponseBody()
        {
            const int userId = 1;
            var user = new UserDto { Id = userId, FirstName = "FirstName111", LastName = "LastName", Email = "test@test.com", Birthday = DateTime.Now.AddYears(-20)};

            var userResult = await HttpClientWrapper.PostAsync<UserDto>(_baseUrlChunck, Client, user);

            var deleteUserResult = await HttpClientWrapper.DeleteAsync(_baseUrlChunck, userId, Client);

            var checkResult = await HttpClientWrapper.GetAsync<UserDto>(_baseUrlChunck, userId, Client);

            Assert.Equal(HttpStatusCode.Created, userResult.Response.StatusCode);
            Assert.Equal(HttpStatusCode.NoContent, deleteUserResult.StatusCode);
            Assert.Equal(HttpStatusCode.NotFound, checkResult.Response.StatusCode);
            Assert.Equal(userId, userResult.Result.Id);
        }

        [Fact]
        public async Task DeleteNotExistingUser_ThanResponseCode404()
        {
            const int userId = 1;

            var deleteResult = await HttpClientWrapper.DeleteAsync(_baseUrlChunck, userId, Client);

            Assert.Equal(HttpStatusCode.NotFound, deleteResult.StatusCode);
        }
    }
}
