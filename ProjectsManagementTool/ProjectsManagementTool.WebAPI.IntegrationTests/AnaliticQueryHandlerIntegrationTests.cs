﻿using CollectionsAndLinq.Services;
using ProjectsManagementTool.Common.DTOs;
using ProjectsManagementTool.Common.Models;
using System;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace ProjectsManagementTool.WebAPI.IntegrationTests
{
    public class AnaliticQueryHandlerIntegrationTests : BaseIntegrationTests
    {
        private const string _userUrlChunck = "api/users";
        private const string _taskUrlChunck = "api/tasks";
        private const string _analiticUrlChunck = "api/analitic";

        public AnaliticQueryHandlerIntegrationTests(CustomWebApplicationFactory<Startup> factory) : base(factory) { }

        [Fact]
        public async Task GetUserNotDoneTasks_When1From3UserTasksDone_Then2TasksWithNotDoneTaskStatus()
        {
            const int userId = 101;
            var user = new UserDto { Id = userId, FirstName = "FirstName111", LastName = "LastName1", Email = "test@test.com", Birthday = DateTime.Now.AddYears(-30) };

            var task1 = new TaskDto { Id = 101, Name = "Name1", Description = "Description1", State = TaskState.Canceled, PerformerId = userId };
            var task2 = new TaskDto { Id = 102, Name = "Name2", Description = "Description2", State = TaskState.Done, PerformerId = userId };
            var task3 = new TaskDto { Id = 103, Name = "Name3", Description = "Description3", State = TaskState.InProgress, PerformerId = userId };

            var userResponse = await HttpClientWrapper.PostAsync<UserDto>(_userUrlChunck, Client, user);

            var task1Response = await HttpClientWrapper.PostAsync<TaskDto>(_taskUrlChunck, Client, task1);
            var task2Response = await HttpClientWrapper.PostAsync<TaskDto>(_taskUrlChunck, Client, task2);
            var task3Response = await HttpClientWrapper.PostAsync<TaskDto>(_taskUrlChunck, Client, task3);

            Assert.Equal(HttpStatusCode.Created, task1Response.Response.StatusCode);
            Assert.Equal(HttpStatusCode.Created, task2Response.Response.StatusCode);
            Assert.Equal(HttpStatusCode.Created, task3Response.Response.StatusCode);

            var result = await HttpClientWrapper.GetListAsync<TaskDto>(
                $"{_analiticUrlChunck}/performerNotDoneTasks/{userId}", Client);

            await HttpClientWrapper.DeleteAsync(_userUrlChunck, userId, Client);
            await HttpClientWrapper.DeleteAsync(_taskUrlChunck, task1.Id, Client);
            await HttpClientWrapper.DeleteAsync(_taskUrlChunck, task2.Id, Client);
            await HttpClientWrapper.DeleteAsync(_taskUrlChunck, task3.Id, Client);

            Assert.Equal(HttpStatusCode.OK, result.Response.StatusCode);
            Assert.Equal(2, result.Result.Count);
            Assert.False(result.Result[0].State == TaskState.Done);
            Assert.False(result.Result[1].State == TaskState.Done);
            Assert.Equal(HttpStatusCode.Created, userResponse.Response.StatusCode);
        }

        [Fact]
        public async Task GetUserNotDoneTasks_When3UserTasksNotDoneAndUserIdNotExist_ThenResponseStatusCode404()
        {
            const int userId = 222;
            var task1 = new TaskDto { Id = 101, Name = "Name1", Description = "Description1", State = TaskState.Canceled, PerformerId = userId };
            var task2 = new TaskDto { Id = 102, Name = "Name2", Description = "Description2", State = TaskState.InProgress, PerformerId = userId };
            var task3 = new TaskDto { Id = 103, Name = "Name3", Description = "Description3", State = TaskState.InProgress, PerformerId = userId };

            await HttpClientWrapper.PostAsync<TaskDto>(_taskUrlChunck, Client, task1);
            await HttpClientWrapper.PostAsync<TaskDto>(_taskUrlChunck, Client, task2);
            await HttpClientWrapper.PostAsync<TaskDto>(_taskUrlChunck, Client, task3);

            var result = await HttpClientWrapper.GetListAsync<TaskDto>(
                $"{_analiticUrlChunck}/performerNotDoneTasks/{userId}", Client);

            await HttpClientWrapper.DeleteAsync(_taskUrlChunck, task1.Id, Client);
            await HttpClientWrapper.DeleteAsync(_taskUrlChunck, task2.Id, Client);
            await HttpClientWrapper.DeleteAsync(_taskUrlChunck, task3.Id, Client);

            Assert.Equal(HttpStatusCode.NotFound, result.Response.StatusCode);
        }

        [Fact]
        public async Task GetUserNotDoneTasks_WhenUserHaveNoNotDoneTasks_ThenResponseStatus200AndEmptyTasksList()
        {
            const int userId = 101;
            var user = new UserDto { Id = userId, FirstName = "FirstName111", LastName = "LastName1", Email = "test@test.com", Birthday = DateTime.Now.AddYears(-30) };

            var userResponse = await HttpClientWrapper.PostAsync<UserDto>(_userUrlChunck, Client, user);

            Assert.Equal(HttpStatusCode.Created, userResponse.Response.StatusCode);

            var result = await HttpClientWrapper.GetListAsync<TaskDto>(
                $"{_analiticUrlChunck}/performerNotDoneTasks/{userId}", Client);

            await HttpClientWrapper.DeleteAsync(_userUrlChunck, userId, Client);

            Assert.Equal(HttpStatusCode.OK, result.Response.StatusCode);
            Assert.Empty(result.Result);
        }
    }
}
