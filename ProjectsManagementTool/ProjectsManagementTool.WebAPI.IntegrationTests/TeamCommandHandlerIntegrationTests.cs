﻿using CollectionsAndLinq.Services;
using ProjectsManagementTool.Common.DTOs;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace ProjectsManagementTool.WebAPI.IntegrationTests
{
    public class TeamCommandHandlerIntegrationTests : BaseIntegrationTests
    {
        private const string _baseUrlChunck = "api/teams";

        public TeamCommandHandlerIntegrationTests(CustomWebApplicationFactory<Startup> factory) : base(factory) { }

        [Fact]
        public async Task AddTeam_ThanResponseCode201AndCorrectResponseBody()
        {
            const int teamId = 1;
            var team = new TeamDto { Id = teamId, Name = "Name111" };

            var teamResult = await HttpClientWrapper.PostAsync<TeamDto>(_baseUrlChunck, Client, team);

            await HttpClientWrapper.DeleteAsync(_baseUrlChunck, teamId, Client);

            Assert.Equal(HttpStatusCode.Created, teamResult.Response.StatusCode);
            Assert.Equal(teamId, teamResult.Result.Id);
            Assert.Equal(team.Name, teamResult.Result.Name);
        }

        [Fact]
        public async Task AddTeamWithExistingId_ThanResponseCode409AndCorrectResponseBody()
        {
            const int teamId = 1;
            var team1 = new TeamDto { Id = teamId, Name = "Name111" };
            var team2 = new TeamDto { Id = teamId, Name = "Name111" };

            var team1Result = await HttpClientWrapper.PostAsync<TeamDto>(_baseUrlChunck, Client, team1);

            var team2Result = await HttpClientWrapper.PostAsync(_baseUrlChunck, Client, team2);

            await HttpClientWrapper.DeleteAsync(_baseUrlChunck, teamId, Client);

            Assert.Equal(HttpStatusCode.Created, team1Result.Response.StatusCode);
            Assert.Equal(HttpStatusCode.Conflict, team2Result.Response.StatusCode);
            Assert.Equal(teamId, team1Result.Result.Id);
            Assert.Equal(team1.Name, team1Result.Result.Name);
        }
    }
}
