﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsAndLinq.Services
{
    public static class HttpClientWrapper
    {
        public static async Task<ClientResult<T>> GetAsync<T>(string requestUri, int id, HttpClient client)
            where T : class
        {
            var response = await client.GetAsync($"{requestUri}/{id}");
            if (!response.IsSuccessStatusCode)
                return new ClientResult<T> { Response = response };

            var objectJsonString = await response.Content.ReadAsStringAsync();
            return new ClientResult<T> { Response = response, Result = JsonConvert.DeserializeObject<T>(objectJsonString) };
        }

        public static async Task<ClientListResult<T>> GetListAsync<T>(string requestUri, HttpClient client)
             where T : class
        {
            var response = await client.GetAsync(requestUri);
            if (!response.IsSuccessStatusCode)
                return new ClientListResult<T> { Response = response };

            var objectJsonString = await response.Content.ReadAsStringAsync();
            return new ClientListResult<T> { Response = response, Result = JsonConvert.DeserializeObject<List<T>>(objectJsonString) };
        }

        public static async Task<ClientResult<T>> PostAsync<T>(string requestUri, HttpClient client, T obj)
             where T : class
        {
            var response = await client.PostAsync(requestUri,
                new StringContent(JsonConvert.SerializeObject(obj),
                Encoding.UTF8,
                "application/json"));

            if (!response.IsSuccessStatusCode)
                return new ClientResult<T> { Response = response };

            var objectJsonString = await response.Content.ReadAsStringAsync();
            return new ClientResult<T> { Response = response, Result = JsonConvert.DeserializeObject<T>(objectJsonString) };
        }

        public static async Task<HttpResponseMessage> PutAsync<T>(string requestUri, HttpClient client, T obj)
        {
            return await client.PutAsync($"{requestUri}/update",
                new StringContent(JsonConvert.SerializeObject(obj),
                Encoding.UTF8,
                "application/json"));
        }

        public static async Task<HttpResponseMessage> DeleteAsync(string requestUri, int id, HttpClient client)
        {
            return await client.DeleteAsync($"{requestUri}/{id}");
        }
    }

    public class ClientResult<T>
    {
        public HttpResponseMessage Response { get; set; }
        public T Result { get; set; }
    }

    public class ClientListResult<T>
    {
        public HttpResponseMessage Response { get; set; }
        public IList<T> Result { get; set; }
    }
}

