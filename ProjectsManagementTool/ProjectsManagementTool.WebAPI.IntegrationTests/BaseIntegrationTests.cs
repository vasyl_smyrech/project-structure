﻿using Microsoft.Extensions.DependencyInjection;
using ProjectsManagementTool.DAL.Context;
using System;
using System.Net.Http;
using Xunit;

namespace ProjectsManagementTool.WebAPI.IntegrationTests
{
    public class BaseIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
        {
        private readonly IServiceScopeFactory _scopeFactory;
        private readonly IServiceScope _scope;
        private readonly PmtContext _context;

        public BaseIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            Client = factory.CreateClient();
            _scopeFactory = factory.Services.GetService<IServiceScopeFactory>();
            _scope = _scopeFactory.CreateScope();
            _context = _scope.ServiceProvider.GetService<PmtContext>();
            _context.Database.EnsureCreated();
        }

        protected HttpClient Client { get; }

        public void Dispose()
        {
            Client.Dispose();
            _context.Dispose();
            _scope.Dispose();
        }
    }
}
