﻿namespace ProjectsManagementTool.BL.Commands.EntitiesCommands.TaskCommands
{
    public class AddMemberToTeamCommand
    {
        public AddMemberToTeamCommand(int teamId, int userId)
        {
            TeamId = teamId;
            UserId = userId;
        }

        public int TeamId { get; }
        public int UserId { get; }
    }
}
