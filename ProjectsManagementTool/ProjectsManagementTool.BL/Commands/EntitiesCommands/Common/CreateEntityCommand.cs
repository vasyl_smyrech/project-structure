﻿namespace ProjectsManagementTool.BL.Commands.EntitiesCommands.Common
{
    public class CreateEntityCommand<TDto>
    {
        public CreateEntityCommand(TDto dto)
        {
            Dto = dto;
        }
        public TDto Dto { get; }
    }
}
