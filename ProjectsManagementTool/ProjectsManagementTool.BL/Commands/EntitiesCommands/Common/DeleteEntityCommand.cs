﻿namespace ProjectsManagementTool.BL.Commands.EntitiesCommands.Common
{
    public class DeleteEntityCommand
    {
        public DeleteEntityCommand(int entityId)
        {
            EntityId = entityId;
        }
        public int EntityId { get; }
    }
}
