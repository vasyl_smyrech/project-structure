﻿namespace ProjectsManagementTool.BL.Commands.EntitiesCommands.Common
{
    public class UpdateEntityCommand<TDto>
    {
        public UpdateEntityCommand(TDto entity)
        {
            Entity = entity;
        }
        public TDto Entity { get; }
    }
}
