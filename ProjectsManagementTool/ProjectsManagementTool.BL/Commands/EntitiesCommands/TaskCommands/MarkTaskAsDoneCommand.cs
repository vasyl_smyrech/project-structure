﻿namespace ProjectsManagementTool.BL.Commands.EntitiesCommands.TaskCommands
{
    public class MarkTaskAsDoneCommand
    {
        public MarkTaskAsDoneCommand(int taskId)
        {
            TaskId = taskId;
        }
        public int TaskId { get; }
    }
}
