﻿namespace ProjectsManagementTool.BL.Commands.LogCommands
{
    public class WriteLogCommand
    {
        public WriteLogCommand(string logInfo)
        {
            LogInfo = logInfo;
        }

        public string LogInfo { get; }
    }
}