﻿using AutoMapper;
using ProjectsManagementTool.BL.Commands.EntitiesCommands.TaskCommands;
using ProjectsManagementTool.BL.Interfaces.CommandHandlers;
using ProjectsManagementTool.BL.Interfaces.QueriesHandlers;
using ProjectsManagementTool.BL.Queries.EntitiesQueries.CommonEntityQueries;
using ProjectsManagementTool.BL.Queries.EntitiesQueries.TeamQueries;
using ProjectsManagementTool.Common.DTOs;
using ProjectsManagementTool.DAL.Entities;
using ProjectsManagementTool.DAL.Interfaces;
using System.Threading.Tasks;

namespace ProjectsManagementTool.BL.Commands.CommandHandlers.EntityComandHandlers
{
    public class TeamCommandHandler : EntityBaseCommandHandler<Team, TeamDto>, ITeamCommandHandler<Team, TeamDto>
    {
        private readonly ITeamQueryHandler<Team, TeamDto> _teamQueryHandler;
        private readonly IUserQueryHandler<User, UserDto> _userQueryHandler;

        public TeamCommandHandler(
            ITeamQueryHandler<Team, TeamDto> teamQueryHandler,
            IUserQueryHandler<User, UserDto> userQueryHandler,
            IPmtContext context, IMapper mapper
            ) : base(context, mapper)
        {
            _teamQueryHandler = teamQueryHandler;
            _userQueryHandler = userQueryHandler;
         }

        public async Task HandleAsync(AddMemberToTeamCommand command)
        {
            var team = await _teamQueryHandler.HandleAsync(new GetTeamWithProjectsAndMembers(command.TeamId));
            var user = await _userQueryHandler.HandleAsync(new GetEntityByIdQuery<User>(command.UserId));
            team.Users.Add(Mapper.Map<User>(user));

            await Context.SaveChangesAsync();
        }
    }
}
