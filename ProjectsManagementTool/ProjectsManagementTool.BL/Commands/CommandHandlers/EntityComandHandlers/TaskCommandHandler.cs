﻿using AutoMapper;
using ProjectsManagementTool.BL.Commands.EntitiesCommands.TaskCommands;
using ProjectsManagementTool.BL.Interfaces.CommandHandlers;
using ProjectsManagementTool.BL.Interfaces.QueriesHandlers;
using ProjectsManagementTool.BL.Queries.EntitiesQueries.CommonEntityQueries;
using ProjectsManagementTool.Common.DTOs;
using ProjectsManagementTool.Common.Models;
using ProjectsManagementTool.DAL.Entities;
using ProjectsManagementTool.DAL.Interfaces;
using System.Threading.Tasks;

namespace ProjectsManagementTool.BL.Commands.CommandHandlers.EntityComandHandlers
{
    public class TaskCommandHandler : EntityBaseCommandHandler<ProjectTask, TaskDto>, ITaskCommandHandler<ProjectTask, TaskDto>
    {
        private readonly ITaskQueryHandler<ProjectTask, TaskDto> _taskQueryHandler;

        public TaskCommandHandler(ITaskQueryHandler<ProjectTask, TaskDto> taskQueryHandler, IPmtContext context, IMapper mapper) : base(context, mapper)
        {
            _taskQueryHandler = taskQueryHandler;
        }

        public async Task HandleAsync(MarkTaskAsDoneCommand command)
        {
            var task = await _taskQueryHandler.HandleAsync(new GetEntityByIdQuery<ProjectTask>(command.TaskId));
            task.State = TaskState.Done;

            await Context.SaveChangesAsync();
        }
    }
}
