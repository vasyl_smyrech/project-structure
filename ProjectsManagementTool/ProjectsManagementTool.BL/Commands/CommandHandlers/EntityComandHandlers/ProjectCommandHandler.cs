﻿using AutoMapper;
using ProjectsManagementTool.BL.Interfaces.CommandHandlers;
using ProjectsManagementTool.Common.DTOs;
using ProjectsManagementTool.DAL.Entities;
using ProjectsManagementTool.DAL.Interfaces;

namespace ProjectsManagementTool.BL.Commands.CommandHandlers.EntityComandHandlers
{
    public class ProjectCommandHandler : EntityBaseCommandHandler<Project, ProjectDto>, IProjectCommandHandler<Project, ProjectDto>
    {
        public ProjectCommandHandler(IPmtContext context, IMapper mapper) : base(context, mapper)
        {
        }
    }
}
