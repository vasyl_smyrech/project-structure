﻿using AutoMapper;
using ProjectsManagementTool.BL.Interfaces.CommandHandlers;
using ProjectsManagementTool.Common.DTOs;
using ProjectsManagementTool.DAL.Entities;
using ProjectsManagementTool.DAL.Interfaces;

namespace ProjectsManagementTool.BL.Commands.CommandHandlers.EntityComandHandlers
{
    public class UserCommandHandler : EntityBaseCommandHandler<User, UserDto>, IUserCommandHandler<User, UserDto>
    {
        public UserCommandHandler(IPmtContext context, IMapper mapper) : base(context, mapper)
        {
        }
    }
}
