﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ProjectsManagementTool.Common.Exceptions;
using ProjectsManagementTool.BL.Interfaces.CommandHandlers;
using System;
using System.Threading.Tasks;
using ProjectsManagementTool.Common.Converters;
using ProjectsManagementTool.BL.Commands.EntitiesCommands.Common;
using ProjectsManagementTool.DAL.Entities;
using ProjectsManagementTool.DAL.Interfaces;

namespace ProjectsManagementTool.BL.Commands.CommandHandlers.EntityComandHandlers
{
    public class EntityBaseCommandHandler<TEntity, TDto> : IEntityBaseCommandHandler<TEntity, TDto>
        where TEntity : BaseEntity
    {
        public EntityBaseCommandHandler(IPmtContext context, IMapper mapper)
        {
            Context = context;
            DbSet = Context.Set<TEntity>();
            Mapper = mapper;
        }

        protected IPmtContext Context { get; }
        public DbSet<TEntity> DbSet { get; set; }
        protected IMapper Mapper { get; }

        public async Task<TDto> HandleAsync(CreateEntityCommand<TDto> command)
        {
            var newEntity = Mapper.Map<TEntity>(command.Dto);

            await DbSet.AddAsync(newEntity);

            await Context.SaveChangesAsync();

            return Mapper.Map<TDto>(newEntity);
        }

        public async Task HandleAsync(UpdateEntityCommand<TDto> command)
        {
            try
            {
                var t = DbSet.Update(Mapper.Map<TEntity>(command.Entity));

                await Context.SaveChangesAsync();
            }
            catch (Exception)
            {
                throw new NotFoundException(TypeToTypeNameConverter.GetSimpleTypeName(typeof(TEntity)));
            }
        }

        public async Task HandleAsync(DeleteEntityCommand command)
        {
            try
            {
                var u = await  DbSet.ToListAsync();
                var removingEntity = await DbSet.FirstAsync(e => e.Id == command.EntityId);
                DbSet.Remove(removingEntity);

                await Context.SaveChangesAsync();
            }
            catch (Exception)
            {
                throw new NotFoundException(TypeToTypeNameConverter.GetSimpleTypeName(typeof(TEntity)));
            }
        }

        public virtual void Dispose()
        {
            Context.Dispose();
        }
    }
}
