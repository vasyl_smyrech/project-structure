﻿using ProjectsManagementTool.BL.Commands.LogCommands;
using ProjectsManagementTool.BL.Interfaces.CommandHandlers;
using System;
using System.IO;

namespace ProjectsManagementTool.BL.Commands.CommandHandlers
{
    public class LogCommandHandler : ILogCommandHandler
    {
        private readonly string _logFilePath;
        private StreamWriter _streamWriter;

        public LogCommandHandler(string logFilePath)
        {
            _logFilePath = logFilePath;
        }

        public void Handle(WriteLogCommand command)
        {
            try
            {
                _streamWriter = new StreamWriter(_logFilePath, true);
                _streamWriter.WriteLine(command.LogInfo);
            }
            catch (Exception e)
            {
                throw new InvalidOperationException(e.Message);
            }
            finally
            {
                _streamWriter?.Dispose();
            }
        }
    }
}
