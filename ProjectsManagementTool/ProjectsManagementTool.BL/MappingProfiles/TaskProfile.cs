﻿using AutoMapper;
using ProjectsManagementTool.Common.DTOs;
using ProjectsManagementTool.DAL.Entities;

namespace ProjectsManagementTool.BL.MappingProfiles
{
    public class TaskProfile : Profile
    {
        public TaskProfile()
        {
            CreateMap<ProjectTask, TaskDto>();
            CreateMap<TaskDto, ProjectTask>();
        }
    }
}
