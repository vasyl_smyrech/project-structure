﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ProjectsManagementTool.BL.Interfaces.QueryHandlers;
using ProjectsManagementTool.BL.Queries.AnaliticQueries;
using ProjectsManagementTool.Common.DTOs;
using ProjectsManagementTool.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectsManagementTool.DAL.Helpers;
using ProjectsManagementTool.DAL.Entities;
using ProjectsManagementTool.Common.Converters;
using ProjectsManagementTool.DAL.Interfaces;

namespace ProjectsManagementTool.BL.Queries.QueryHandlers
{
    public class AnaliticQueryHandler : IAnaliticQueryHandler
    {
        private readonly IPmtContext _context;
        private readonly IMapper _mapper;

        public AnaliticQueryHandler(IPmtContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public async Task<ProjectTasksCountByProjectDto> HandleAsync(GetProjectTasksCountByAuthorIdQuery query)
        {
            await EntityHalper.GetEntityByIdIfExistOrThrow<User>(_context, query.AuthorId, "Author");

            var projects = await _context.Projects.
             Include(p => p.Tasks).ToListAsync();

                var tasksCountByProject = projects
                   .Where(p => p.AuthorId == query.AuthorId)
                   .ToDictionary(p => _mapper.Map<ProjectDto>(p), p => p.Tasks.Count);

            return new ProjectTasksCountByProjectDto
            {
                TasksCountByProject = tasksCountByProject
            };
        }

        public async Task<IList<TaskDto>> HandleAsync(GetPerformerTasksByNameLengthLess45Query query)
        {
            await EntityHalper.GetEntityByIdIfExistOrThrow<User>(_context, query.PerformerId, "Performer");

            return _mapper.Map<List<TaskDto>>(await _context.Tasks
               .Where(t => t.PerformerId == query.PerformerId && t.Name.Length < 45)
               .ToListAsync());
        }

        public async Task<IList<ShortTaskInfoDto>> HandleAsync(GetPerformerFinishedTasksByYearQuery query)
        {
            await EntityHalper.GetEntityByIdIfExistOrThrow<User>(_context, query.PerformerId, "Performer");

            return await _context.Tasks
                .Where(t => t.PerformerId == query.PerformerId && t.FinishedAt.Year == 2020)
                .Select(t => new ShortTaskInfoDto
                {
                    TaskId = t.Id,
                    TaskName = t.Name
                })
                .ToListAsync();
        }

        public async Task<IList<TeamWithMembersDto>> HandleAsync(GetTeamsAndMembersFilteredByMembersAgeQuery query)
        {
            var teams = await _context.Teams.ToListAsync();
            var users = await _context.Users.ToListAsync();

            return teams
                .Join(
                    users
                    .Where(u => u.Birthday < DateTime.Now.AddYears(-10) && u.TeamId != null)
                    .OrderByDescending(m => m.RegisteredAt)
                    .GroupBy(m => m.TeamId)
                    .ToDictionary(g => g.Key, g => g.ToList()),
                t => t.Id,
                g => g.Key,
                (t, g) => (new TeamWithMembersDto
                {
                    TeamId = t.Id,
                    TeamName = t.Name,
                    Members = _mapper.Map<List<UserDto>>(g.Value)
                })
                ).ToList();
        }

        public async Task<IList<UserWithTasksDto>> HandleAsync(GetUsersByFirstNameWithTasksSortedByNameLengthQuery _)
        {
            var users = await _context.Users.ToListAsync();
            var userTasks = await _context.Tasks.ToListAsync();

            return users
                .OrderBy(u => u.FirstName)
                .GroupJoin(userTasks, u => u.Id, t => t.PerformerId,
                    (user, usersTasks) => (new UserWithTasksDto
                    {
                        Performer = _mapper.Map<UserDto>(user),
                        Tasks = _mapper.Map<List<TaskDto>>(usersTasks.OrderByDescending(t => t.Name.Length).ToList())
                    }))
                .ToList();
        }

        public async Task<UserAnaliticInfoDto> HandleAsync(GetUserAnaliticInfoQuery query)
        {
            var users = new List<User>() { await EntityHalper.GetEntityByIdIfExistOrThrow<User>(
                _context, query.UserId, TypeToTypeNameConverter.GetSimpleTypeName(typeof(User))) };
            var tasks = await _context.Tasks.Where(t => t.PerformerId == query.UserId).ToListAsync();
            var projects = await _context.Projects.Include(p => p.Tasks).Where(p => p.AuthorId == query.UserId).ToListAsync();

            return users
                .Where(u => u.Id == query.UserId)
                .GroupJoin(tasks, u => u.Id, t => t.PerformerId, (user, userTasks) => new { User = user, UserTasks = userTasks })
                .GroupJoin(projects, (data) => data.User.Id, p => p.AuthorId,
                (data, userProjects) =>
                    new UserAnaliticInfoDto()
                    {
                        User = _mapper.Map<UserDto>(data.User),
                        LastProject = _mapper.Map<ProjectDto>(userProjects.OrderByDescending(p => p.CreatedAt).FirstOrDefault()),
                        LastProjectTasksAmount = !userProjects.OrderByDescending(p => p.CreatedAt).Any()
                            ? 0
                            : userProjects.OrderByDescending(p => p.CreatedAt).First().Tasks.Count,
                        CanceledOrNotDoneTasksAmount = data.UserTasks.Count(t => t.State == TaskState.Canceled || t.State == TaskState.InProgress),
                        MostTimeConsumingTask = _mapper.Map<TaskDto>(data.UserTasks.OrderBy(t => t.FinishedAt - t.CreatedAt).FirstOrDefault())
                    })
                .ToList().FirstOrDefault();
        }

        public async Task<IList<ProjectAnaliticInfoDto>> HandleAsync(GetProjectsAnaliticInfoQuery _)
        {
            return await _context.Projects
                .Include(p => p.Author)
                .Include(p => p.Tasks)
                .Include(p => p.Team)
                .Select(p => GetProjectAnaliticInfo(p, _mapper))
                .ToListAsync();
        }

        public async Task<IList<TaskDto>> HandleAsync(GetPerformerNotDoneTasksQuery query)
        {
            await EntityHalper.GetEntityByIdIfExistOrThrow<User>(_context, query.PerformerId, "Performer");

            return _mapper.Map<List<TaskDto>>(await _context.Tasks
               .Where(t => t.PerformerId == query.PerformerId && t.State != TaskState.Done)
               .ToListAsync());
        }

        private static ProjectAnaliticInfoDto GetProjectAnaliticInfo(Project p, IMapper mapper)
        {
            return new ProjectAnaliticInfoDto
            {
                Project = mapper.Map<ProjectDto>(p),
                TaskWithLongestDescription = mapper.Map<TaskDto>(p.Tasks.OrderByDescending(t => t.Description.Length).FirstOrDefault()),
                TaskWithShortestName = mapper.Map<TaskDto>(p.Tasks.OrderBy(t => t.Name.Length).FirstOrDefault()),
                MembersCountByTasksCountAndProjDescLength =
                    p.Description.Length > 20 || p.Tasks.Count < 3 ? p.Tasks.Select(t => t.Performer).Distinct().Count() : 0
            };
        }
    }
}
