﻿using ProjectsManagementTool.BL.Interfaces.QueryHandlers;
using ProjectsManagementTool.BL.Queries.LogQueries;
using System;
using System.IO;

namespace ProjectsManagementTool.BL.Queries.QueryHandlers
{
    public class LogQueryHandler : ILogQueryHandler
    {
        private readonly string _logFilePath;
        private StreamReader _streamReader;

        public LogQueryHandler(string logFilePath)
        {
            _logFilePath = logFilePath;
        }

        public string Handle(ReadLogQuery command)
        {
            try
            {
                _streamReader = new StreamReader(_logFilePath);
                return _streamReader.ReadToEnd();
            }
            catch (Exception e)
            {
                throw new InvalidOperationException(e.Message);
            }
            finally
            {
                _streamReader?.Dispose();
            }
        }
    }
}
