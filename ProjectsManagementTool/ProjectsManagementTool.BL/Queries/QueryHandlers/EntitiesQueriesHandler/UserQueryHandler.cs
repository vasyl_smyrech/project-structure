﻿using AutoMapper;
using ProjectsManagementTool.BL.Interfaces.QueriesHandlers;
using ProjectsManagementTool.BL.Queries.QueryHandlers;
using ProjectsManagementTool.Common.DTOs;
using ProjectsManagementTool.DAL.Entities;
using ProjectsManagementTool.DAL.Interfaces;

namespace ProjectsManagementTool.BL.Commands.CommandHandlers.EntityComandHandlers
{
    public class UserQueryHandler : EntityBaseQueryHandler<User, UserDto>, IUserQueryHandler<User, UserDto>
    {
        public UserQueryHandler(IPmtContext context, IMapper mapper) : base(context, mapper)
        {
        }
    }
}
