﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ProjectsManagementTool.BL.Interfaces.QueriesHandlers;
using ProjectsManagementTool.BL.Queries.EntitiesQueries.TeamQueries;
using ProjectsManagementTool.BL.Queries.QueryHandlers;
using ProjectsManagementTool.Common.DTOs;
using ProjectsManagementTool.DAL.Entities;
using ProjectsManagementTool.DAL.Interfaces;
using System.Threading.Tasks;

namespace ProjectsManagementTool.BL.Commands.CommandHandlers.EntityComandHandlers
{
    public class TeamQueryHandler : EntityBaseQueryHandler<Team, TeamDto>, ITeamQueryHandler<Team, TeamDto>
    {
        public TeamQueryHandler(IPmtContext context, IMapper mapper) : base(context, mapper) { }

        public async Task<Team> HandleAsync(GetTeamWithProjectsAndMembers query)
        {
            return await DbSet
                .Include(t => t.Projects)
                .Include(t => t.Users)
                .FirstAsync(t => t.Id == query.TeamId);
        }
    }
}
