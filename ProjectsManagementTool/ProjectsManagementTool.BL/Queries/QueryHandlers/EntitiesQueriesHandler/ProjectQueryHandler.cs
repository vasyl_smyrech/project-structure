﻿using AutoMapper;
using ProjectsManagementTool.BL.Interfaces.QueriesHandlers;
using ProjectsManagementTool.BL.Queries.QueryHandlers;
using ProjectsManagementTool.Common.DTOs;
using ProjectsManagementTool.DAL.Entities;
using ProjectsManagementTool.DAL.Interfaces;

namespace ProjectsManagementTool.BL.Commands.CommandHandlers.EntityComandHandlers
{
    public class ProjectQueryHandler : EntityBaseQueryHandler<Project, ProjectDto>, IProjectQueryHandler<Project, ProjectDto>
    {
        public ProjectQueryHandler(IPmtContext context, IMapper mapper) : base(context, mapper)
        {
        }
    }
}
