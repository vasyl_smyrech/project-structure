﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ProjectsManagementTool.BL.Interfaces.QueriesHandlers;
using ProjectsManagementTool.BL.Queries.EntitiesQueries.CommonEntityQueries.TaskQueries;
using ProjectsManagementTool.BL.Queries.QueryHandlers;
using ProjectsManagementTool.Common.DTOs;
using ProjectsManagementTool.Common.Models;
using ProjectsManagementTool.DAL.Entities;
using ProjectsManagementTool.DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectsManagementTool.BL.Commands.CommandHandlers.EntityComandHandlers
{
    public class TaskQueryHandler : EntityBaseQueryHandler<ProjectTask, TaskDto>, ITaskQueryHandler<ProjectTask, TaskDto>
    {
        public TaskQueryHandler(IPmtContext context, IMapper mapper) : base(context, mapper) { }

        public async Task<IList<TaskDto>> HandleAsync(GetAllNotDoneTasksQuery query)
        {
            return Mapper.Map<List<TaskDto>>(await Context.Tasks.Where(t => t.State != TaskState.Done).ToListAsync());
        }
    }
}
