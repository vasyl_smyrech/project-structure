﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ProjectsManagementTool.Common.Exceptions;
using ProjectsManagementTool.BL.Interfaces.QueryHandlers.EntityQueryHandlers;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ProjectsManagementTool.Common.Converters;
using ProjectsManagementTool.BL.Queries.EntitiesQueries.CommonEntityQueries;
using ProjectsManagementTool.DAL.Entities;
using ProjectsManagementTool.DAL.Interfaces;

namespace ProjectsManagementTool.BL.Queries.QueryHandlers
{
    public class EntityBaseQueryHandler<TEntity, TDto> : IEntityBaseQueryHandler<TEntity, TDto>
        where TEntity : BaseEntity
    {
        public EntityBaseQueryHandler(IPmtContext context, IMapper mapper)
        {
            Context = context;
            DbSet = context.Set<TEntity>();
            Mapper = mapper;
        }

        protected IPmtContext Context { get; }
        protected DbSet<TEntity> DbSet { get; }
        protected IMapper Mapper { get; }

        public async Task<IEnumerable<TDto>> HandleAsync(GetAllEntitiesQuery _)
        {
            return Mapper.Map<List<TDto>>(await DbSet.AsNoTracking().ToListAsync());
        }

        public async Task<TDto> HandleAsync(GetEntityByIdQuery<TDto> query)
        {
            try
            {
                return Mapper.Map<TDto>(await DbSet.AsNoTracking().FirstAsync(u => u.Id == query.EntityId));
            }
            catch (Exception)
            {
                throw new NotFoundException(TypeToTypeNameConverter.GetSimpleTypeName(typeof(TEntity)));
            }
        }

        public async Task<TEntity> HandleAsync(GetEntityByIdQuery<TEntity> query)
        {
            try
            {
                return await DbSet.FirstAsync(u => u.Id == query.EntityId);
            }
            catch (Exception)
            {
                throw new NotFoundException(TypeToTypeNameConverter.GetSimpleTypeName(typeof(TEntity)));
            }
        }

        public virtual void Dispose()
        {
            Context.Dispose();
        }
    }
}
