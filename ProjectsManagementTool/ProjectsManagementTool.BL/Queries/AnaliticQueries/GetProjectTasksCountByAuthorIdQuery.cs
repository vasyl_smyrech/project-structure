﻿namespace ProjectsManagementTool.BL.Queries.AnaliticQueries
{
    public class GetProjectTasksCountByAuthorIdQuery
    {
        public GetProjectTasksCountByAuthorIdQuery(int authorId)
        {
            AuthorId = authorId;
        }

        public int AuthorId { get; }
    }
}
