﻿namespace ProjectsManagementTool.BL.Queries.AnaliticQueries
{
    public class GetPerformerFinishedTasksByYearQuery
    {
        public GetPerformerFinishedTasksByYearQuery(int performerId)
        {
            PerformerId = performerId;
        }

        public int PerformerId { get; }
    }
}
