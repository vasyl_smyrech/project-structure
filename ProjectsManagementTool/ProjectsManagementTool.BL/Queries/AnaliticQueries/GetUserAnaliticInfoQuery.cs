﻿namespace ProjectsManagementTool.BL.Queries.AnaliticQueries
{
    public class GetUserAnaliticInfoQuery
    {
        public GetUserAnaliticInfoQuery(int userId)
        {
            UserId = userId;
        }

        public int UserId { get; }
    }
}
