﻿namespace ProjectsManagementTool.BL.Queries.AnaliticQueries
{
    public class GetPerformerNotDoneTasksQuery
    {
        public GetPerformerNotDoneTasksQuery(int performerId)
        {
            PerformerId = performerId;
        }

        public int PerformerId { get; }
    }
}
