﻿namespace ProjectsManagementTool.BL.Queries.AnaliticQueries
{
    public class GetPerformerTasksByNameLengthLess45Query
    {
        public GetPerformerTasksByNameLengthLess45Query(int performerId)
        {
            PerformerId = performerId;
        }

        public int PerformerId { get; }
    }
}
