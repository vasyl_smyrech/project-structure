﻿namespace ProjectsManagementTool.BL.Queries.EntitiesQueries.TeamQueries
{
    public class GetTeamWithProjectsAndMembers
    {
        public GetTeamWithProjectsAndMembers(int teamId)
        {
            TeamId = teamId;
        }

        public int TeamId { get; }
    }
}
