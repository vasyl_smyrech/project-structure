﻿namespace ProjectsManagementTool.BL.Queries.EntitiesQueries.CommonEntityQueries
{
    public class GetEntityByIdQuery<T>
    {
        public GetEntityByIdQuery(int entityId)
        {
            EntityId = entityId;
        }

        public int EntityId { get; }
    }
}
