﻿using ProjectsManagementTool.BL.Queries.LogQueries;

namespace ProjectsManagementTool.BL.Interfaces.QueryHandlers
{
    public interface ILogQueryHandler
    {
        string Handle(ReadLogQuery command);
    }
}
