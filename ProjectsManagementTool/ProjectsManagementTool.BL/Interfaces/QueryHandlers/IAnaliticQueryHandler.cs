﻿using ProjectsManagementTool.BL.Queries.AnaliticQueries;
using ProjectsManagementTool.Common.DTOs;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectsManagementTool.BL.Interfaces.QueryHandlers
{
    public interface IAnaliticQueryHandler : IDisposable
    {
        Task<IList<ProjectAnaliticInfoDto>> HandleAsync(GetProjectsAnaliticInfoQuery _);
        Task<UserAnaliticInfoDto> HandleAsync(GetUserAnaliticInfoQuery query);
        Task<IList<UserWithTasksDto>> HandleAsync(GetUsersByFirstNameWithTasksSortedByNameLengthQuery _);
        Task<IList<TeamWithMembersDto>> HandleAsync(GetTeamsAndMembersFilteredByMembersAgeQuery _);
        Task<IList<ShortTaskInfoDto>> HandleAsync(GetPerformerFinishedTasksByYearQuery query);
        Task<IList<TaskDto>> HandleAsync(GetPerformerTasksByNameLengthLess45Query query);
        Task<ProjectTasksCountByProjectDto> HandleAsync(GetProjectTasksCountByAuthorIdQuery query);
        Task<IList<TaskDto>> HandleAsync(GetPerformerNotDoneTasksQuery getPerformerNotDoneTasksQuery);
    }
}
