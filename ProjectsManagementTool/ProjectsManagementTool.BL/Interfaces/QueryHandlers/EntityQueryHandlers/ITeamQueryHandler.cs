﻿using ProjectsManagementTool.BL.Interfaces.QueryHandlers.EntityQueryHandlers;
using ProjectsManagementTool.BL.Queries.EntitiesQueries.TeamQueries;
using ProjectsManagementTool.Common.DTOs;
using ProjectsManagementTool.DAL.Entities;
using System.Threading.Tasks;

namespace ProjectsManagementTool.BL.Interfaces.QueriesHandlers
{
    public interface ITeamQueryHandler<TEntity, TDto> : IEntityBaseQueryHandler<TEntity, TDto>
        where TEntity : Team where TDto : TeamDto
    {
        //place specific team queries here
        Task<Team> HandleAsync(GetTeamWithProjectsAndMembers query);
    }
}