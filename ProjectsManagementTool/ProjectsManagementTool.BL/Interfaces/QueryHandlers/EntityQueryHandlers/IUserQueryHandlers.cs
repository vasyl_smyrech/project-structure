﻿using ProjectsManagementTool.BL.Interfaces.QueryHandlers.EntityQueryHandlers;
using ProjectsManagementTool.Common.DTOs;
using ProjectsManagementTool.DAL.Entities;

namespace ProjectsManagementTool.BL.Interfaces.QueriesHandlers
{
    public interface IUserQueryHandler<TEntity, TDto> : IEntityBaseQueryHandler<TEntity, TDto>
        where TEntity : User where TDto : UserDto
    {
        //place specific user queries here
    }
}
