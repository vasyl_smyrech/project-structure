﻿using ProjectsManagementTool.BL.Interfaces.QueryHandlers.EntityQueryHandlers;
using ProjectsManagementTool.Common.DTOs;
using ProjectsManagementTool.DAL.Entities;

namespace ProjectsManagementTool.BL.Interfaces.QueriesHandlers
{
    public interface IProjectQueryHandler<TEntity, TDto> : IEntityBaseQueryHandler<TEntity, TDto>
        where TEntity : Project where TDto : ProjectDto
    {
        //place specific project queries here
    }
}