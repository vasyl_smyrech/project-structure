﻿using ProjectsManagementTool.BL.Queries.EntitiesQueries.CommonEntityQueries;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectsManagementTool.BL.Interfaces.QueryHandlers.EntityQueryHandlers
{
    public interface IEntityBaseQueryHandler<TEntity, TDto> : IDisposable
    {
        Task<IEnumerable<TDto>> HandleAsync(GetAllEntitiesQuery _);
        Task<TEntity> HandleAsync(GetEntityByIdQuery<TEntity> query);
        Task<TDto> HandleAsync(GetEntityByIdQuery<TDto> query);
    }
}
