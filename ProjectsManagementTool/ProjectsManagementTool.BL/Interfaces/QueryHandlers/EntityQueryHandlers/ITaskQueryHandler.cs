﻿using ProjectsManagementTool.BL.Interfaces.QueryHandlers.EntityQueryHandlers;
using ProjectsManagementTool.BL.Queries.EntitiesQueries.CommonEntityQueries.TaskQueries;
using ProjectsManagementTool.Common.DTOs;
using ProjectsManagementTool.DAL.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectsManagementTool.BL.Interfaces.QueriesHandlers
{
    public interface ITaskQueryHandler<TEntity, TDto> : IEntityBaseQueryHandler<TEntity, TDto>
        where TEntity : ProjectTask where TDto : TaskDto
    {
        //place specific task queries here
        Task<IList<TaskDto>> HandleAsync(GetAllNotDoneTasksQuery query);
    }
}