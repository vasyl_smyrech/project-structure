﻿using ProjectsManagementTool.BL.Commands.EntitiesCommands.TaskCommands;
using ProjectsManagementTool.Common.DTOs;
using ProjectsManagementTool.DAL.Entities;
using System.Threading.Tasks;

namespace ProjectsManagementTool.BL.Interfaces.CommandHandlers
{
    public interface ITaskCommandHandler<TEntity, TDto> : IEntityBaseCommandHandler<TEntity, TDto>
        where TEntity : ProjectTask where TDto : TaskDto
    {
        //place specific task commands here
        Task HandleAsync(MarkTaskAsDoneCommand command);
    }
}
