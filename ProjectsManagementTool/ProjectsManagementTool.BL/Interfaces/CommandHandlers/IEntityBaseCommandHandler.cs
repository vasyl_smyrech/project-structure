﻿using Microsoft.EntityFrameworkCore;
using ProjectsManagementTool.BL.Commands.EntitiesCommands.Common;
using System;
using System.Threading.Tasks;

namespace ProjectsManagementTool.BL.Interfaces.CommandHandlers
{
    public interface IEntityBaseCommandHandler<TEntity, TDto> : IDisposable where TEntity : class
    {
        DbSet<TEntity> DbSet { get; }
        Task<TDto> HandleAsync(CreateEntityCommand<TDto> command);
        Task HandleAsync(UpdateEntityCommand<TDto> command);
        Task HandleAsync(DeleteEntityCommand command);
    }
}
