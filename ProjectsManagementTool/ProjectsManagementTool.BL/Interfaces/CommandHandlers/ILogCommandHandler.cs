﻿using ProjectsManagementTool.BL.Commands.LogCommands;

namespace ProjectsManagementTool.BL.Interfaces.CommandHandlers
{
    public interface ILogCommandHandler
    {
        void Handle(WriteLogCommand command);
    }
}
