﻿using ProjectsManagementTool.BL.Commands.EntitiesCommands.TaskCommands;
using ProjectsManagementTool.Common.DTOs;
using ProjectsManagementTool.DAL.Entities;
using System.Threading.Tasks;

namespace ProjectsManagementTool.BL.Interfaces.CommandHandlers
{
    public interface ITeamCommandHandler<TEntity, TDto> : IEntityBaseCommandHandler<TEntity, TDto>
        where TEntity : Team where TDto : TeamDto
    {
        //place specific team commands here
        Task HandleAsync(AddMemberToTeamCommand addMemberToTeamCommand);
    }
}
