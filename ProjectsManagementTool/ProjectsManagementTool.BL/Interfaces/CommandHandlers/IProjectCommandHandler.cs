﻿using ProjectsManagementTool.Common.DTOs;
using ProjectsManagementTool.DAL.Entities;

namespace ProjectsManagementTool.BL.Interfaces.CommandHandlers
{
    public interface IProjectCommandHandler<TEntity, TDto> : IEntityBaseCommandHandler<TEntity, TDto>
        where TEntity : Project where TDto : ProjectDto
    {
        //place specific project commands here
    }
}
