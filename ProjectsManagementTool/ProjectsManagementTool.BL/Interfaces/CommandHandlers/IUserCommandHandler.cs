﻿using ProjectsManagementTool.Common.DTOs;
using ProjectsManagementTool.DAL.Entities;

namespace ProjectsManagementTool.BL.Interfaces.CommandHandlers
{
    public interface IUserCommandHandler<TEntity, TDto> : IEntityBaseCommandHandler<TEntity, TDto>
        where TEntity : User where TDto : UserDto
    {
        //place specific user commands here
    }
}
