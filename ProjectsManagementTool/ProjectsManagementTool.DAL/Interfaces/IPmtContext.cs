﻿using Microsoft.EntityFrameworkCore;
using ProjectsManagementTool.DAL.Entities;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectsManagementTool.DAL.Interfaces
{
    public interface IPmtContext : IDisposable
    {
        DbSet<TEntity> Set<TEntity>() where TEntity : class;
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
        DbSet<User> Users { get; set; }
        DbSet<Project> Projects { get; set; }
        DbSet<ProjectTask> Tasks { get; set; }
        DbSet<Team> Teams { get; set; }
        int SaveChanges();
    }
}