﻿using System;
using System.Collections.Generic;

namespace ProjectsManagementTool.DAL.Entities
{
    public class Project : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }

        public int? AuthorId { get; set; }
        public User Author { get; set; }

        public int? TeamId { get; set; }
        public Team Team { get; set; }

        public List<ProjectTask> Tasks { get; set; }
    }
}
