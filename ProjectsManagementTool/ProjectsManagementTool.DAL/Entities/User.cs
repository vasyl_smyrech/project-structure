﻿using System;

namespace ProjectsManagementTool.DAL.Entities
{
    public class User : BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime RegisteredAt { get => CreatedAt; }

        public int? TeamId { get; set; }
        public Team Team { get; set; }
    }
}
