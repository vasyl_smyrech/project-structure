﻿using ProjectsManagementTool.Common.Models;
using System;

namespace ProjectsManagementTool.DAL.Entities
{
    public class ProjectTask : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime FinishedAt { get; set; }
        public TaskState State { get; set; }
        public int ProjectId { get; set; }

        public int? PerformerId { get; set; }
        public User Performer { get; set; }
    }
}
