﻿using System.Collections.Generic;

namespace ProjectsManagementTool.DAL.Entities
{
    public class Team : BaseEntity
    {
        public string Name { get; set; }
        public ICollection<Project> Projects { get; } = new List<Project>();
        public ICollection<User> Users { get; } = new List<User>();
    }
}
