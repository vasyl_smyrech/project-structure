﻿using Microsoft.EntityFrameworkCore;
using ProjectsManagementTool.Common.Exceptions;
using ProjectsManagementTool.DAL.Entities;
using ProjectsManagementTool.DAL.Interfaces;
using System;
using System.Threading.Tasks;

namespace ProjectsManagementTool.DAL.Helpers
{
    public static class EntityHalper
    {
        public static async Task<TEntity> GetEntityByIdIfExistOrThrow<TEntity>(
            IPmtContext context, int entityId, string typeName)
            where TEntity : BaseEntity
        {
            try
            {
                return await context.Set<TEntity>().FirstAsync(u => u.Id == entityId);
            }
            catch (InvalidOperationException)
            {
                throw new NotFoundException(typeName, entityId);
            }
        }
    }
}
