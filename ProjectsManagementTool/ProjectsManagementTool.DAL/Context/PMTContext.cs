﻿using Microsoft.EntityFrameworkCore;
using ProjectsManagementTool.DAL.Entities;
using ProjectsManagementTool.DAL.Interfaces;

namespace ProjectsManagementTool.DAL.Context
{
    public class PmtContext : DbContext, IPmtContext
    {
        public PmtContext(DbContextOptions<PmtContext> options) : base(options)
        {
        }

        public static bool ShouldDbBeSeeded = true;
        public DbSet<Project> Projects { get; set; }
        public DbSet<ProjectTask> Tasks { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Configure();

            if(ShouldDbBeSeeded)
                 modelBuilder.Seed();
        }
    }
}
