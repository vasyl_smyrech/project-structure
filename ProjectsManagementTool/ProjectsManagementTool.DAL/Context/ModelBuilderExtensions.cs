﻿using Bogus;
using Microsoft.EntityFrameworkCore;
using ProjectsManagementTool.Common.Models;
using ProjectsManagementTool.DAL.Entities;
using System;
using System.Collections.Generic;

namespace ProjectsManagementTool.DAL.Context
{
    public static class ModelBuilderExtensions
    {
        private const int USER_COUNT = 100;
        private const int PROJECT_COUNT = 150;
        private const int TASK_COUNT = 300;
        private const int TEAM_COUNT = 50;

        public static void Configure(this ModelBuilder modelBuilder)
        {
            //Team
            modelBuilder.Entity<Team>()
                .HasMany(t => t.Projects)
                .WithOne()
                .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<Team>()
                .HasMany(t => t.Projects)
                .WithOne(t => t.Team)
                .HasForeignKey(p => p.TeamId);

            modelBuilder.Entity<Team>()
                .HasMany(t => t.Users)
                .WithOne()
                .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<Team>()
                .HasMany(t => t.Users)
                .WithOne()
                .HasForeignKey(p => p.TeamId);

            modelBuilder.Entity<Team>()
                .Property(u => u.Name)
                .HasMaxLength(200)
                .IsRequired();

            //User
            modelBuilder.Entity<User>()
                .HasOne(u => u.Team)
                .WithMany(u => u.Users)
                .HasForeignKey(u => u.TeamId);

            modelBuilder.Entity<User>()
                .Ignore(u => u.RegisteredAt)
                .Property(u => u.FirstName)
                .HasMaxLength(100)
                .IsRequired();

            modelBuilder.Entity<User>()
                .Property(u => u.LastName)
                .HasMaxLength(300)
                .IsRequired();

            modelBuilder.Entity<User>()
                .Property(u => u.Email)
                .HasMaxLength(300)
                .IsRequired();

            //Project
            modelBuilder.Entity<Project>()
                .HasOne(pr => pr.Author)
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Project>()
                .HasMany(pr => pr.Tasks)
                .WithOne()
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Project>()
                .HasOne(t => t.Author)
                .WithMany()
                .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<Project>()
                .Property(u => u.Name)
                .HasMaxLength(200)
                .IsRequired();

            modelBuilder.Entity<Project>()
                .Property(u => u.Description)
                .HasMaxLength(1000);

            //Task
            modelBuilder.Entity<ProjectTask>()
                .HasOne(t => t.Performer)
                .WithMany()
                .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<ProjectTask>()
                .Property(u => u.Name)
                .HasMaxLength(300)
                .IsRequired();

            modelBuilder.Entity<ProjectTask>()
                .Property(u => u.Description)
                .HasMaxLength(1000);

            modelBuilder.Entity<ProjectTask>()
                .Property(u => u.State)
                .HasDefaultValue(TaskState.ToDo);
        }

        public static void Seed(this ModelBuilder modelBuilder)
        {
            var teams = GenerateRandomTeams();
            var users = GenerateRandomUsers(teams);
            var projects = GenerateRandomProjects(users, teams);
            var tasks = GenerateRandomTasks(users, projects);

            modelBuilder.Entity<Team>().HasData(teams);
            modelBuilder.Entity<User>().HasData(users);
            modelBuilder.Entity<Project>().HasData(projects);
            modelBuilder.Entity<ProjectTask>().HasData(tasks);
        }

        private static ICollection<Team> GenerateRandomTeams()
        {
            var id = 1;

            var faker = new Faker<Team>()
                .RuleFor(p => p.Id, _ => id++)
                .RuleFor(p => p.Name, f => f.Lorem.Sentence(1, 2))
                .RuleFor(p => p.CreatedAt, f => f.Date.Between(DateTime.Now.AddYears(-5), DateTime.Now.AddYears(-1)));

            return faker.Generate(TEAM_COUNT);
        }

        public static ICollection<User> GenerateRandomUsers(this ICollection<Team> teams)
        {
            var id = 1;

            var faker = new Faker<User>()
                .RuleFor(u => u.Id, _ => id++)
                .RuleFor(u => u.FirstName, f => f.Person.FirstName)
                .RuleFor(u => u.LastName, f => f.Person.LastName)
                .RuleFor(u => u.Email, f => f.Internet.Email())
                .RuleFor(u => u.Birthday, f => f.Date.Between(DateTime.Now.AddYears(-60), DateTime.Now.AddYears(-8)))
                .RuleFor(u => u.RegisteredAt, f => f.Date.Between(DateTime.Now.AddYears(-6), DateTime.Now.AddYears(-1)))
                .RuleFor(u => u.TeamId, f => f.PickRandom(teams).Id);

            return faker.Generate(USER_COUNT);
        }

        public static ICollection<Project> GenerateRandomProjects(this ICollection<User> users, ICollection<Team> teams)
        {
            var id = 1;

            var faker = new Faker<Project>()
                .RuleFor(p => p.Id, _ => id++)
                .RuleFor(p => p.Name, f => f.Lorem.Sentence(1, 6))
                .RuleFor(p => p.Description, f => f.Lorem.Paragraph())
                .RuleFor(p => p.AuthorId, f => f.PickRandom(users).Id)
                .RuleFor(p => p.TeamId, f => f.PickRandom(teams).Id)
                .RuleFor(p => p.CreatedAt, f => f.Date.Between(DateTime.Now.AddYears(-5), DateTime.Now.AddYears(-1)))
                .RuleFor(p => p.Deadline, f => f.Date.Between(DateTime.Now.AddYears(-1), DateTime.Now.AddYears(2)));

            return faker.Generate(PROJECT_COUNT);
        }

        public static ICollection<ProjectTask> GenerateRandomTasks(this ICollection<User> users, ICollection<Project> projects)
        {
            var id = 1;

            var faker = new Faker<ProjectTask>()
                .RuleFor(p => p.Id, _ => id++)
                .RuleFor(p => p.Name, f => f.Lorem.Sentence(1, 8))
                .RuleFor(p => p.Description, f => f.Lorem.Paragraph())
                .RuleFor(p => p.PerformerId, f => f.PickRandom(users).Id)
                .RuleFor(p => p.ProjectId, f => f.PickRandom(projects).Id)
                .RuleFor(p => p.State, f => f.PickRandom<TaskState>())
                .RuleFor(p => p.CreatedAt, f => f.Date.Between(DateTime.Now.AddYears(-5), DateTime.Now.AddYears(-1)))
                .RuleFor(p => p.FinishedAt, f => f.Date.Between(DateTime.Now.AddYears(-2), DateTime.Now));

            return faker.Generate(TASK_COUNT);
        }
    }
}
