﻿namespace ProjectsManagementTool.BL.Tests.Extensions
{
    public static class StringExtensions
    {
        /// <summary>
        /// Define if parameter alphabetically plased before method caller
        /// </summary>
        /// <param name="str"></param>
        /// <param name="compareString"></param>
        /// <returns>True if  parameter preceids or same lavel as method caller
        /// if sorted the strings alphabetically ascending</returns>
        public static bool AlphabeticallyPreceidOrSameLevel(this string str, string compareString)
        {
            var compareResult = string.Compare(compareString, str);
            return compareResult == -1 || compareResult == 0;
        }
    }
}
