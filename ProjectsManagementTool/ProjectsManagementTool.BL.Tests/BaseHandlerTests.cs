﻿using AutoMapper;
using FakeItEasy;
using Microsoft.EntityFrameworkCore;
using ProjectsManagementTool.BL.MappingProfiles;
using ProjectsManagementTool.DAL.Context;
using ProjectsManagementTool.DAL.Interfaces;
using System;

namespace ProjectsManagementTool.BL.Tests
{
    public abstract class BaseHandlerTests : IDisposable
    {
        protected BaseHandlerTests()
        {
            Mapper = new Mapper(new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<ProjectProfile>();
                cfg.AddProfile<TaskProfile>();
                cfg.AddProfile<UserProfile>();
                cfg.AddProfile<TeamProfile>();
            }));

            FakeContext = A.Fake<IPmtContext>();

            PmtContext.ShouldDbBeSeeded = false;
            Context = new PmtContext(ContextOptions);

            Context.Database.EnsureDeleted();
            Context.Database.EnsureCreated();

            CreateHandler();
        }

        protected Mapper Mapper { get; }

        protected DbContextOptions<PmtContext> ContextOptions => new DbContextOptionsBuilder<PmtContext>()
                    .UseInMemoryDatabase($"TestDatabase{new Guid()}")
                    .EnableServiceProviderCaching(false)
                    .EnableDetailedErrors(true)
                    .Options;

        protected PmtContext Context { get; }
        protected IPmtContext FakeContext { get; }

        protected abstract void CreateHandler();

        public virtual void Dispose()
        {
            Context.Dispose();
        }
    }
}
