﻿using ProjectsManagementTool.BL.Commands.CommandHandlers.EntityComandHandlers;
using ProjectsManagementTool.BL.Commands.EntitiesCommands.TaskCommands;
using ProjectsManagementTool.DAL.Entities;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace ProjectsManagementTool.BL.Tests
{
    public class TeamCommandHandlerTests : BaseHandlerTests
    {
        private TeamCommandHandler _commandHandler;

        protected override void CreateHandler()
        {
            _commandHandler = new TeamCommandHandler(
                new TeamQueryHandler(Context, Mapper),
                new UserQueryHandler(Context, Mapper),
                Context,
                Mapper);
        }

        [Fact]
        public async Task AddMamberToTeam_WhenNewCommandAddNewUser_ThenUpdateInDbContextCalledOnce()
        {
            var team = new Team { Id = 1, Name = "Name" };
            var user = new User { Id = 1, FirstName = "FN", LastName = "LN", Birthday = DateTime.Now.AddYears(-20) };

            await Context.Teams.AddAsync(team);
            await Context.Users.AddAsync(user);

            await Context.SaveChangesAsync();

            await _commandHandler.HandleAsync(new AddMemberToTeamCommand(team.Id, user.Id));

            Assert.True(Context.Teams.AsQueryable().First(t => t.Id == team.Id).Users.First(u => u.Id == user.Id).TeamId == team.Id );
        }

        public override void Dispose()
        {
            base.Dispose();
            _commandHandler.Dispose();
        }
    }
}
