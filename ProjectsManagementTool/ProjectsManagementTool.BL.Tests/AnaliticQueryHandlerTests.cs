﻿using ProjectsManagementTool.BL.Queries.AnaliticQueries;
using ProjectsManagementTool.BL.Queries.QueryHandlers;
using ProjectsManagementTool.BL.Tests.Extensions;
using ProjectsManagementTool.Common.DTOs;
using ProjectsManagementTool.Common.Exceptions;
using ProjectsManagementTool.Common.Models;
using ProjectsManagementTool.DAL.Entities;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace ProjectsManagementTool.BL.Tests
{
    public class AnaliticQueryHandlerTests : BaseHandlerTests
    {
        private AnaliticQueryHandler _handler;

        [Fact]
        public async Task GetProjectTasksCountByAuthorIdQuery_WhenSeededAndId3_Then2ProjectsWith3TasksInEachOne()
        {
            await Seed();
            var dto = await _handler.HandleAsync(new GetProjectTasksCountByAuthorIdQuery(3));

            Assert.IsType<ProjectDto>(dto.TasksCountByProject.Keys.First());
            Assert.IsType<int>(dto.TasksCountByProject.Values.First());
            Assert.Equal(2, dto.TasksCountByProject.Count);
            Assert.Equal(3, dto.TasksCountByProject.Values.First());
            Assert.Equal(3, dto.TasksCountByProject.Values.Last());
            Assert.Equal(3, dto.TasksCountByProject.Keys.First().AuthorId);
            Assert.Equal(3, dto.TasksCountByProject.Keys.Last().AuthorId);
        }

        private async Task Seed()
        {
            await Context.Database.EnsureDeletedAsync();

            await Context.Users.AddRangeAsync(
                new User { Id = 1, FirstName = "Sergii", LastName = "Sergiev", Email = "sergii@test.com", Birthday = new DateTime(1990, 01, 01), TeamId = 2 },
                new User { Id = 2, FirstName = "Ivan", LastName = "Ivanov", Email = "ivan@test.com", Birthday = new DateTime(1991, 01, 01), TeamId = 1 },
                new User { Id = 3, FirstName = "vasyl", LastName = "Vasyliev", Email = "vasyl@test.com", Birthday = new DateTime(1992, 02, 02), TeamId = 1 }
                );

            await Context.Teams.AddRangeAsync(
                new Team { Id = 1, Name = "First team" },
                new Team { Id = 2, Name = "Second team" }
               );

            await Context.Projects.AddRangeAsync(
                new Project { Id = 1, Name = "Project1 ", Description = "Project description 1", AuthorId = 2, TeamId = 1, Deadline = DateTime.Now.AddMonths(3) },
                new Project { Id = 2, Name = "Project22", Description = "Project description 22", AuthorId = 2, TeamId = 2, Deadline = DateTime.Now.AddMonths(2) },
                new Project { Id = 3, Name = "Project333", Description = "Project description 333", AuthorId = 3, TeamId = 2, Deadline = DateTime.Now.AddMonths(-1) },
                new Project { Id = 4, Name = "Project4444", Description = "Project description 4444", AuthorId = 3, TeamId = 2, Deadline = DateTime.Now.AddMonths(3) }
               );

            await Context.Tasks.AddRangeAsync(
                new ProjectTask { Id = 1, Name = "Name12121212121212121212121212121212121212121212121", Description = "1", State = TaskState.ToDo, FinishedAt = new DateTime(), PerformerId = 1, ProjectId = 1 },
                new ProjectTask { Id = 2, Name = "Name88888888", Description = "22", State = TaskState.Done, FinishedAt = new DateTime(2020, 02, 02), PerformerId = 1, ProjectId = 1 },
                new ProjectTask { Id = 3, Name = "Name999999999", Description = "333", State = TaskState.InProgress, FinishedAt = new DateTime(), PerformerId = 1, ProjectId = 1 },
                new ProjectTask { Id = 4, Name = "Name1010101010", Description = "4444", State = TaskState.InProgress, FinishedAt = new DateTime(), PerformerId = 3, ProjectId = 3 },
                new ProjectTask { Id = 5, Name = "Name11111111111", Description = "1010101010", State = TaskState.InProgress, FinishedAt = new DateTime(), PerformerId = 2, ProjectId = 3 },
                new ProjectTask { Id = 6, Name = "Name7777777", Description = "11111111111", State = TaskState.Done, FinishedAt = DateTime.Now.AddDays(-10), PerformerId = 2, ProjectId = 4 },
                new ProjectTask { Id = 7, Name = "Name1", Description = "121212121212", State = TaskState.Done, FinishedAt = DateTime.Now.AddDays(-20), PerformerId = 2, ProjectId = 4 },
                new ProjectTask { Id = 8, Name = "Name22", Description = "55555", State = TaskState.ToDo, FinishedAt = new DateTime(), PerformerId = 2, ProjectId = 4 },
                new ProjectTask { Id = 9, Name = "Name333", Description = "666666", State = TaskState.Done, FinishedAt = DateTime.Now.AddMonths(-3), PerformerId = 2, ProjectId = 2 },
                new ProjectTask { Id = 10, Name = "Name4444", Description = "7777777", State = TaskState.Done, FinishedAt = DateTime.Now.AddMonths(-4), PerformerId = 2, ProjectId = 2 },
                new ProjectTask { Id = 11, Name = "Name55555", Description = "88888888", State = TaskState.Canceled, FinishedAt = new DateTime(), PerformerId = 3, ProjectId = 3 },
                new ProjectTask { Id = 12, Name = "Name666666", Description = "999999999", State = TaskState.Canceled, FinishedAt = new DateTime(), PerformerId = 2, ProjectId = 2 }
                );

            await Context.SaveChangesAsync();
        }

        [Fact]
        public async Task GetProjectTasksCountByAuthorIdQuery_WhenNotExistingUserId_ThenThrowNotFoundException()
        {
            await Assert.ThrowsAsync<NotFoundException>(async ()
                => await _handler.HandleAsync(new GetProjectTasksCountByAuthorIdQuery(0)));
        }

        [Fact]
        public async Task GetPerformerTasksByNameLengthLess45Query_WhenSeededAndId1_Then2TasksWithNamesLess45()
        {
            await Seed();

            var taskDtos = await _handler.HandleAsync(new GetPerformerTasksByNameLengthLess45Query(1));

            Assert.Equal(2, taskDtos.Count);
            Assert.True(taskDtos[0].Name.Length < 45);
            Assert.True(taskDtos[1].Name.Length < 45);
        }

        [Fact]
        public async Task GetPerformerTasksByNameLengthLess45Query_WhenNotExistingUserId_ThenThrowNotFoundException()
        {
            await Assert.ThrowsAsync<NotFoundException>(async ()
                => await _handler.HandleAsync(new GetPerformerTasksByNameLengthLess45Query(0)));
        }

        [Fact]
        public async Task GetPerformerFinishedTasksByYearQuery_WhenSeededAndId1_Then2TasksWithNamesLess45()
        {
            await Seed();

            var shortTaskInfoDtos = await _handler.HandleAsync(new GetPerformerFinishedTasksByYearQuery(1));

            Assert.Equal(1, shortTaskInfoDtos.Count);
            Assert.Equal(2, shortTaskInfoDtos[0].TaskId);
        }

        [Fact]
        public async Task GetPerformerFinishedTasksByYearQuery_WhenNotExistingUserId_ThenThrowNotFoundException()
        {
            await Assert.ThrowsAsync<NotFoundException>(async ()
                => await _handler.HandleAsync(new GetPerformerFinishedTasksByYearQuery(0)));
        }

        [Fact]
        public async Task GetTeamsAndMembersFilteredByMembersAgeQuery_WhenSeeded_ThenFirstTeamWith2MembersSecondOneWith1Member()
        {
            await Seed();

            var teamWithMembersDtos = await _handler.HandleAsync(new GetTeamsAndMembersFilteredByMembersAgeQuery());

            Assert.Equal(2, teamWithMembersDtos.Count);
            Assert.Equal(1, teamWithMembersDtos[0].TeamId);
            Assert.Equal(2, teamWithMembersDtos[0].Members.Count);
            Assert.Equal(3, teamWithMembersDtos[0].Members[0].Id);
            Assert.Equal(2, teamWithMembersDtos[0].Members[1].Id);
            Assert.Equal(2, teamWithMembersDtos[1].TeamId);
            Assert.Equal(1, teamWithMembersDtos[1].Members[0].Id);
        }

        [Fact]
        public async Task GetUsersByFirstNameWithTasksSortedByNameLengthQuery_WhenSeeded_ThenUsersSortedByFirstNameWithTasksSortedByNameLength()
        {
            await Seed();

            var usersWithTasksDtos = await _handler.HandleAsync(new GetUsersByFirstNameWithTasksSortedByNameLengthQuery());

            Assert.Equal(3, usersWithTasksDtos.Count);

            //Check if users sorted alphabetically ascending by 'FirstName'
            Assert.True(usersWithTasksDtos[1].Performer.FirstName
                .AlphabeticallyPreceidOrSameLevel(usersWithTasksDtos[0].Performer.FirstName));
            Assert.True(usersWithTasksDtos[2].Performer.FirstName
               .AlphabeticallyPreceidOrSameLevel(usersWithTasksDtos[1].Performer.FirstName));

            //Check if tasks sorted descending by 'Name'
            Assert.True(usersWithTasksDtos[1].Tasks[0].Name.Length > usersWithTasksDtos[1].Tasks[1].Name.Length);
            Assert.True(usersWithTasksDtos[1].Tasks[1].Name.Length > usersWithTasksDtos[1].Tasks[2].Name.Length);
        }

        [Fact]
        public async Task GetUserAnaliticInfoQuery_WhenSeededAndId2_ThenDtoUserId2LastProjectId2With3Tasks()
        {
            await Seed();

            const int userId = 2;
            var userAnaliticInfoDto = await _handler.HandleAsync(new GetUserAnaliticInfoQuery(userId));

            Assert.Equal(userId, userAnaliticInfoDto.User.Id);
            Assert.Equal(2, userAnaliticInfoDto.LastProject.Id);
            Assert.Equal(3, userAnaliticInfoDto.LastProjectTasksAmount);
            Assert.Equal(2, userAnaliticInfoDto.CanceledOrNotDoneTasksAmount);
            Assert.Equal(12, userAnaliticInfoDto.MostTimeConsumingTask.Id);
        }

        [Fact]
        public async Task GetUserAnaliticInfoQuery_WhenSeededAndNotExistingUserId_ThenThrowNotFoundException()
        {
            await Assert.ThrowsAsync<NotFoundException>(async ()
                => await _handler.HandleAsync(new GetUserAnaliticInfoQuery(0)));
        }

        [Fact]
        public async Task GetProjectsAnaliticInfoQuery_WhenSeeded_Then4DtosFirstDtoProjectId1TaskWithShortestNameId2TaskWithLongestDescriptionId3()
        {
            await Seed();

            var projectAnaliticInfoDtos = await _handler.HandleAsync(new GetProjectsAnaliticInfoQuery());

            Assert.Equal(4, projectAnaliticInfoDtos.Count);
            Assert.Equal(1, projectAnaliticInfoDtos[0].Project.Id);
            Assert.Equal(3, projectAnaliticInfoDtos[0].TaskWithLongestDescription.Id);
            Assert.Equal(2, projectAnaliticInfoDtos[0].TaskWithShortestName.Id);
        }

        [Fact]
        public async Task GetPerformerNotDoneTasks_WhenSeeded_Then2TaskWithNotDoneTaskState()
        {
            await Seed();

            const int perfornerId = 1;

            var tasks = await _handler.HandleAsync(new GetPerformerNotDoneTasksQuery(perfornerId));

            Assert.Equal(2, tasks.Count);
            Assert.Equal(1, tasks[0].PerformerId);
            Assert.Equal(1, tasks[1].PerformerId);
            Assert.Equal(1, tasks[0].Id);
            Assert.Equal(3, tasks[1].Id);
            Assert.False(tasks[0].State == TaskState.Done);
            Assert.False(tasks[1].State == TaskState.Done);
        }

        [Fact]
        public async Task GetPerformerNotDoneTasks_WhenSeededAndNotExistingUserId_ThenThrowNotFoundException()
        {
            await Assert.ThrowsAsync<NotFoundException>(async ()
                => await _handler.HandleAsync(new GetPerformerNotDoneTasksQuery(0)));
        }

        protected override void CreateHandler()
        {
            _handler = new AnaliticQueryHandler(Context, Mapper);
        }

        public override void Dispose()
        {
            base.Dispose();
            _handler.Dispose();
        }
    }
}
