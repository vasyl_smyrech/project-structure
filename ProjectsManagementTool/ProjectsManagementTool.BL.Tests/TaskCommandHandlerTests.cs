﻿using ProjectsManagementTool.BL.Commands.CommandHandlers.EntityComandHandlers;
using ProjectsManagementTool.BL.Commands.EntitiesCommands.TaskCommands;
using ProjectsManagementTool.Common.Exceptions;
using ProjectsManagementTool.Common.Models;
using ProjectsManagementTool.DAL.Entities;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace ProjectsManagementTool.BL.Tests
{
    public class TaskCommandHandlerTests : BaseHandlerTests
    {
        private TaskCommandHandler _commandHandler;
        private TaskQueryHandler _queryHandler;

        protected override void CreateHandler()
        {
            _commandHandler = new TaskCommandHandler(
                new TaskQueryHandler(Context, Mapper), Context, Mapper);
            _queryHandler = new TaskQueryHandler(Context, Mapper);
        }

        [Fact]
        public async Task MarkTaskAsDone_WhenNewUserWithTaskStateToDo_ThenUserTaskStateChangedToDone()
        {
            const int taskId = 1;
            await Context.Tasks.AddAsync(
                new ProjectTask { Id = taskId, Name = "Name", Description = "Description", State = TaskState.ToDo, FinishedAt = DateTime.Now }
                );
            await Context.SaveChangesAsync();

            await _commandHandler.HandleAsync(new MarkTaskAsDoneCommand(taskId));

            Assert.True(Context.Tasks.First(t => t.Id == taskId).State == TaskState.Done);
        }

        [Fact]
        public async Task MarkTaskAsDone_WhenTaskIdNotExcist_ThenThrowNotFoundException()
        {
            await Assert.ThrowsAsync<NotFoundException>(
                async () => await _commandHandler.HandleAsync(new MarkTaskAsDoneCommand(0)));
        }

        public override void Dispose()
        {
            _commandHandler.Dispose();
            _queryHandler.Dispose();
        }
    }
}
