﻿using AutoMapper;
using FakeItEasy;
using Microsoft.EntityFrameworkCore;
using ProjectsManagementTool.BL.Commands.CommandHandlers.EntityComandHandlers;
using ProjectsManagementTool.BL.Commands.EntitiesCommands.Common;
using ProjectsManagementTool.Common.DTOs;
using ProjectsManagementTool.DAL.Entities;
using ProjectsManagementTool.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace ProjectsManagementTool.BL.Tests
{
    public class UserCommandHandlerTests : BaseHandlerTests
    {
        private UserCommandHandler _commandHandler;

        protected override void CreateHandler()
        {
            _commandHandler = new UserCommandHandler(Context, Mapper);
        }

        [Fact]
        public async Task AddUser_WhenExistingUserId_ThenThrowArgumentException()
        {
            const int userId = 1;
            var user1 = new UserDto { Id = userId, FirstName = "Sergii", LastName = "Sergiev", Email = "sergii@test.com", Birthday = new DateTime(1990, 01, 01) };
            var user2 = new UserDto { Id = userId, FirstName = "Ivan", LastName = "Ivanov", Email = "ivan@test.com", Birthday = new DateTime(1991, 01, 01) };

            await _commandHandler.HandleAsync(new CreateEntityCommand<UserDto>(user1));
            var l = await Context.Users.ToListAsync();
            await Assert.ThrowsAsync<InvalidOperationException>(async () => await _commandHandler.HandleAsync(new CreateEntityCommand<UserDto>(user2)));
        }

        [Fact]
        public async Task AddUser_WhenNewUser_ThenSingleUserInDbSet()
        {
            var user = new UserDto { FirstName = "Sergii", LastName = "Sergiev", Email = "sergii@test.com", Birthday = new DateTime(1990, 01, 01) };
            var fakeUsersDbSet = A.Fake<DbSet<User>>(d => d.Implements(typeof(IQueryable<User>)).Implements(typeof(IDbAsyncEnumerable<User>)));
            var fakeContext = A.Fake<IPmtContext>();
            var fakeMapper = A.Fake<IMapper>();
            var fakeCommandHandler = A.Fake<UserCommandHandler>(x
                => x.WithArgumentsForConstructor(new List<object>() { fakeContext, fakeMapper }));
            fakeCommandHandler.DbSet = fakeUsersDbSet;

            await fakeCommandHandler.HandleAsync(new CreateEntityCommand<UserDto>(user));

            A.CallTo(() => fakeCommandHandler.DbSet.AddAsync(A<User>.That.Matches(u => u.Id == user.Id), default)).MustHaveHappenedOnceExactly();
            A.CallTo(() => fakeContext.SaveChangesAsync(default)).MustHaveHappenedOnceExactly();
        }

        public override void Dispose()
        {
            base.Dispose();
            _commandHandler.Dispose();
        }
    }
}
