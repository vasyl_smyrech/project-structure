using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ProjectsManagementTool.WebAPI.Extensions;
using ProjectsManagementTool.DAL.Context;
using Microsoft.EntityFrameworkCore;

namespace ProjectsManagementTool.WebAPI
{
    public class Startup
    {
        private readonly string _allowedOrigins = "_alowedOrigins";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<PmtContext>(options
                => options.UseSqlServer(Configuration.GetConnectionString("PMTDBConnection")),
                ServiceLifetime.Scoped);
            services.RegisterCustomServices();
            services.RegisterAutoMapper();
            services.AddCors(options =>
            {
                options.AddPolicy(name: _allowedOrigins,
                    builder =>
                    {
                        builder
                            .AllowAnyMethod()
                            .AllowAnyHeader()
                            .WithOrigins("http://localhost:4200");
                    });
            });
            services.AddControllers().AddNewtonsoftJson();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseStaticFiles();

            app.UseRouting();

            app.UseCors(_allowedOrigins);

            app.UseEndpoints(endpoints => endpoints.MapControllers());
        }
    }
}
