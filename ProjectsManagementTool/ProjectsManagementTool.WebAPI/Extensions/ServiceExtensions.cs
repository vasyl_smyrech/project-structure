﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using ProjectsManagementTool.BL.Commands.CommandHandlers;
using ProjectsManagementTool.BL.Commands.CommandHandlers.EntityComandHandlers;
using ProjectsManagementTool.BL.Interfaces.CommandHandlers;
using ProjectsManagementTool.BL.Interfaces.QueriesHandlers;
using ProjectsManagementTool.BL.Interfaces.QueryHandlers;
using ProjectsManagementTool.BL.Interfaces.QueryHandlers.EntityQueryHandlers;
using ProjectsManagementTool.BL.MappingProfiles;
using ProjectsManagementTool.BL.Queries.QueryHandlers;
using ProjectsManagementTool.Common.DTOs;
using ProjectsManagementTool.DAL.Context;
using ProjectsManagementTool.DAL.Entities;
using ProjectsManagementTool.DAL.Interfaces;
using System.IO;
using System.Reflection;

namespace ProjectsManagementTool.WebAPI.Extensions
{
    public static class ServiceExtensions
    {
        public static void RegisterCustomServices(this IServiceCollection services)
        {
            var logFilePath = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Logs.log";

            services
                .AddScoped<IPmtContext, PmtContext>()
                .AddScoped<ILogCommandHandler>(_ => new LogCommandHandler(logFilePath))
                .AddScoped<IEntityBaseCommandHandler<User, UserDto>, EntityBaseCommandHandler<User, UserDto>>()
                .AddScoped<IEntityBaseCommandHandler<Project, ProjectDto>, EntityBaseCommandHandler<Project, ProjectDto>>()
                .AddScoped<IEntityBaseCommandHandler<ProjectTask, TaskDto>, EntityBaseCommandHandler<ProjectTask, TaskDto>>()
                .AddScoped<IEntityBaseCommandHandler<Team, TeamDto>, EntityBaseCommandHandler<Team, TeamDto>>()
                .AddScoped<IUserCommandHandler<User, UserDto>, UserCommandHandler>()
                .AddScoped<IProjectCommandHandler<Project, ProjectDto>, ProjectCommandHandler>()
                .AddScoped<ITaskCommandHandler<ProjectTask, TaskDto>, TaskCommandHandler>()
                .AddScoped<ITeamCommandHandler<Team, TeamDto>, TeamCommandHandler>()
                .AddScoped<ILogQueryHandler>(_ => new LogQueryHandler(logFilePath))
                .AddScoped<IEntityBaseQueryHandler<User, UserDto>, EntityBaseQueryHandler<User, UserDto>>()
                .AddScoped<IEntityBaseQueryHandler<Project, ProjectDto>, EntityBaseQueryHandler<Project, ProjectDto>>()
                .AddScoped<IEntityBaseQueryHandler<ProjectTask, TaskDto>, EntityBaseQueryHandler<ProjectTask, TaskDto>>()
                .AddScoped<IEntityBaseQueryHandler<Team, TeamDto>, EntityBaseQueryHandler<Team, TeamDto>>()
                .AddScoped<IUserQueryHandler<User, UserDto>, UserQueryHandler>()
                .AddScoped<IProjectQueryHandler<Project, ProjectDto>, ProjectQueryHandler>()
                .AddScoped<ITaskQueryHandler<ProjectTask, TaskDto>, TaskQueryHandler>()
                .AddScoped<ITeamQueryHandler<Team, TeamDto>, TeamQueryHandler>()
                .AddScoped<IAnaliticQueryHandler, AnaliticQueryHandler>();
        }

        public static void RegisterAutoMapper(this IServiceCollection services)
        {
            services.AddAutoMapper(cfg =>
            {
                cfg.AddProfile<ProjectProfile>();
                cfg.AddProfile<TaskProfile>();
                cfg.AddProfile<UserProfile>();
                cfg.AddProfile<TeamProfile>();
            },
            Assembly.GetExecutingAssembly());
        }
    }
}
