﻿using Microsoft.AspNetCore.Mvc;
using ProjectsManagementTool.Common.Exceptions;
using ProjectsManagementTool.BL.Interfaces.QueryHandlers;
using ProjectsManagementTool.BL.Queries.AnaliticQueries;
using ProjectsManagementTool.Common.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectsManagementTool.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class AnaliticController : ControllerBase
    {
        private readonly IAnaliticQueryHandler _queryHandler;

        public AnaliticController(IAnaliticQueryHandler queryHandler)
        {
            _queryHandler = queryHandler;
        }

        [HttpGet("projectTasksCountByProjectAuthorId/{authorId}")]
        public async Task<ActionResult<ProjectTasksCountByProjectDto>> GetProjectTasksCountByProjectAuthorId(int authorId)
        {
            try
            {
                return Ok(await _queryHandler.HandleAsync(new GetProjectTasksCountByAuthorIdQuery(authorId)));
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpGet("performerTasksByNameLength/{performerId}")]
        public async Task<ActionResult<IList<TaskDto>>> GetPerformerTasksByNameLength(int performerId)
        {
            try
            {
                return Ok(await _queryHandler.HandleAsync(new GetPerformerTasksByNameLengthLess45Query(performerId)));
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpGet("performerFinishedTasksByYear/{performerId}")]
        public async Task<ActionResult<IList<ShortTaskInfoDto>>> GetPerformerFinishedTasksByYear(int performerId)
        {
            try
            {
                return Ok(await _queryHandler.HandleAsync(new GetPerformerFinishedTasksByYearQuery(performerId)));
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpGet("teamsAndMembersFilteredByMembersAge")]
        public async Task<ActionResult<List<TeamWithMembersDto>>> GetTeamsAndMembersFilteredByMembersAge()
        {
            return Ok(await _queryHandler.HandleAsync(new GetTeamsAndMembersFilteredByMembersAgeQuery()));
        }

        [HttpGet("usersByFirstNameWithTasksSortedByNameLength")]
        public async Task<ActionResult<List<UserWithTasksDto>>> GetUsersByFirstNameWithTasksSortedByNameLength()
        {
            return Ok(await _queryHandler.HandleAsync(new GetUsersByFirstNameWithTasksSortedByNameLengthQuery()));
        }

        [HttpGet("userAnaliticInfo/{userId}")]
        public async Task<ActionResult<UserAnaliticInfoDto>> GetUserAnaliticInfo(int userId)
        {
            try
            {
                return Ok(await _queryHandler.HandleAsync(new GetUserAnaliticInfoQuery(userId)));
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpGet("projectsAnaliticInfo")]
        public async Task<ActionResult<List<ProjectAnaliticInfoDto>>> GetProjectsAnaliticInfo()
        {
            return Ok(await _queryHandler.HandleAsync(new GetProjectsAnaliticInfoQuery()));
        }

        [HttpGet("performerNotDoneTasks/{performerId}")]
        public async Task<ActionResult<IList<TaskDto>>> GetPerformerNotDoneTasks(int performerId)
        {
            try
            {
                return Ok(await _queryHandler.HandleAsync(new GetPerformerNotDoneTasksQuery(performerId)));
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
        }
    }
}
