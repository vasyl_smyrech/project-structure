﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProjectsManagementTool.BL.Commands.EntitiesCommands.TaskCommands;
using ProjectsManagementTool.BL.Interfaces.CommandHandlers;
using ProjectsManagementTool.BL.Interfaces.QueryHandlers.EntityQueryHandlers;
using ProjectsManagementTool.Common.DTOs;
using ProjectsManagementTool.Common.Exceptions;
using ProjectsManagementTool.DAL.Entities;
using System;
using System.Threading.Tasks;

namespace ProjectsManagementTool.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class TeamsController : BaseController<Team, TeamDto>
    {
        private readonly ITeamCommandHandler<Team, TeamDto> _teamCommandHandler;

        public TeamsController(
            ITeamCommandHandler<Team, TeamDto> teamCommandHandler,
            IEntityBaseCommandHandler<Team, TeamDto> commandHandler,
            IEntityBaseQueryHandler<Team, TeamDto> queryHandler
            ) : base(commandHandler, queryHandler)
        {
            _teamCommandHandler = teamCommandHandler;
        }

        [HttpPut("addMemberToTeam/{teamId}/{userId}")]
        public async Task<ActionResult> AddMemberToTeam(int teamId, int userId)
        {
            try
            {
                await _teamCommandHandler.HandleAsync(new AddMemberToTeamCommand(teamId, userId));
                return Ok();
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex);
            }
        }
    }
}
