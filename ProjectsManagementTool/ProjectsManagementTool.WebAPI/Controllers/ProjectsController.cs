﻿using Microsoft.AspNetCore.Mvc;
using ProjectsManagementTool.BL.Interfaces.CommandHandlers;
using ProjectsManagementTool.BL.Interfaces.QueryHandlers.EntityQueryHandlers;
using ProjectsManagementTool.Common.DTOs;
using ProjectsManagementTool.DAL.Entities;

namespace ProjectsManagementTool.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class ProjectsController : BaseController<Project, ProjectDto>
    {
        public ProjectsController(
            IEntityBaseCommandHandler<Project, ProjectDto> commandHandler,
            IEntityBaseQueryHandler<Project, ProjectDto> queryHandler
            ) : base (commandHandler, queryHandler)
        { }
    }
}
