﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProjectsManagementTool.BL.Commands.EntitiesCommands.Common;
using ProjectsManagementTool.Common.Exceptions;
using ProjectsManagementTool.BL.Interfaces.CommandHandlers;
using ProjectsManagementTool.BL.Interfaces.QueryHandlers.EntityQueryHandlers;
using ProjectsManagementTool.BL.Queries.EntitiesQueries.CommonEntityQueries;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectsManagementTool.WebAPI.Controllers
{
    public class BaseController<TEntity, TDto> : ControllerBase where TEntity : class
    {
        private readonly IEntityBaseCommandHandler<TEntity, TDto> _commandHandler;
        private readonly IEntityBaseQueryHandler<TEntity, TDto> _queryHandler;

        public BaseController(
            IEntityBaseCommandHandler<TEntity, TDto> commandHandler,
            IEntityBaseQueryHandler<TEntity, TDto> queryHandler)
        {
            _commandHandler = commandHandler;
            _queryHandler = queryHandler;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<TDto>>> GetAsync()
        {
            return Ok(await _queryHandler.HandleAsync(new GetAllEntitiesQuery()));
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<TDto>> GetByIdAsync(int id)
        {
            try
            {
                return Ok(await _queryHandler.HandleAsync(new GetEntityByIdQuery<TDto>(id)));
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpPost]
        public async Task<ActionResult<TDto>> CreateAsync(TDto dto)
        {
            try
            {
                return Created(string.Empty, await _commandHandler.HandleAsync(new CreateEntityCommand<TDto>(dto)));
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (ArgumentException ex)
            {
                return Conflict(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex);
            }
        }

        [HttpPut]
        public async Task<ActionResult> UpdateAsync(TDto dto)
        {
            try
            {
                await _commandHandler.HandleAsync(new UpdateEntityCommand<TDto>(dto));
                return Ok();
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            try
            {
                await _commandHandler.HandleAsync(new DeleteEntityCommand(id));
                return NoContent();
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }
    }
}
