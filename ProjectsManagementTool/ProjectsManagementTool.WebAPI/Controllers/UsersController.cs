﻿using Microsoft.AspNetCore.Mvc;
using ProjectsManagementTool.BL.Interfaces.CommandHandlers;
using ProjectsManagementTool.BL.Interfaces.QueryHandlers.EntityQueryHandlers;
using ProjectsManagementTool.Common.DTOs;
using ProjectsManagementTool.DAL.Entities;

namespace ProjectsManagementTool.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class UsersController : BaseController<User, UserDto>
    {
        public UsersController(
            IEntityBaseCommandHandler<User, UserDto> commandHandler,
            IEntityBaseQueryHandler<User, UserDto> queryHandler
            ) : base(commandHandler, queryHandler) { }
    }
}
