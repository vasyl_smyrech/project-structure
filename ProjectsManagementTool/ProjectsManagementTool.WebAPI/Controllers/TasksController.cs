﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProjectsManagementTool.BL.Commands.EntitiesCommands.TaskCommands;
using ProjectsManagementTool.BL.Interfaces.CommandHandlers;
using ProjectsManagementTool.BL.Interfaces.QueriesHandlers;
using ProjectsManagementTool.BL.Queries.EntitiesQueries.CommonEntityQueries.TaskQueries;
using ProjectsManagementTool.Common.DTOs;
using ProjectsManagementTool.Common.Exceptions;
using ProjectsManagementTool.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectsManagementTool.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class TasksController : BaseController<ProjectTask, TaskDto>
    {
        private readonly ITaskCommandHandler<ProjectTask, TaskDto> _taskCommandHandler;
        private readonly ITaskQueryHandler<ProjectTask, TaskDto> _taskQueryHandler;

        public TasksController(
            ITaskCommandHandler<ProjectTask, TaskDto> taskCommandHandler,
            IEntityBaseCommandHandler<ProjectTask, TaskDto> commandHandler,
            ITaskQueryHandler<ProjectTask, TaskDto> queryHandler
            ) : base(commandHandler, queryHandler)
        {
            _taskCommandHandler = taskCommandHandler;
            _taskQueryHandler = queryHandler;
        }

        [HttpGet("allNotDoneTasks")]
        public async Task<ActionResult<IList<TaskDto>>> GetAllNotDoneTasks()
        {
            try
            {
                return Ok(await _taskQueryHandler.HandleAsync(new GetAllNotDoneTasksQuery()));
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex);
            }
        }

        [HttpPut("markTaskAsDone")]
        public async Task<ActionResult> MarkTaskAsDone([FromBody] int taskId)
        {
            try
            {
                await _taskCommandHandler.HandleAsync(new MarkTaskAsDoneCommand(taskId));
                return Ok();
            }
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex);
            }
        }
    }
}
