﻿using System;

namespace ProjectsManagementTool.Common.Exceptions
{
    public class NotFoundException : Exception
    {
        public NotFoundException(string nameOrText, bool shouldUseTextWrapper = true)
            : base((shouldUseTextWrapper ? $"{nameOrText} was not found" : nameOrText).Trim('\"')) { }

        public NotFoundException(string name, int id)
            : base(($"{name} with id {id} was not found").Trim('\"')) { }
    }
}
