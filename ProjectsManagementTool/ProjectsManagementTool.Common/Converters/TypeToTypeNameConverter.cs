﻿using System;
namespace ProjectsManagementTool.Common.Converters
{
    public static class TypeToTypeNameConverter
    {
        public static string GetSimpleTypeName(Type type)
        {
            return type.ToString().Split('.')[^1];
        }
    }
}
