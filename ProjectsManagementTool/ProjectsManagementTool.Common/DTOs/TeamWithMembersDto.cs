﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ProjectsManagementTool.Common.DTOs
{
    public class TeamWithMembersDto
    {
        [JsonProperty("teamId", Required = Required.Always)]
        public int TeamId { get; set; }

        [JsonProperty("teamName", Required = Required.Always)]
        public string TeamName { get; set; }

        [JsonProperty("members", Required = Required.Always)]
        public List<UserDto> Members {get; set; }
    }
}
