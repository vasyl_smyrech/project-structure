﻿using Newtonsoft.Json;

namespace ProjectsManagementTool.Common.DTOs
{
    public class UserAnaliticInfoDto
    {
        [JsonProperty("user", Required = Required.Always)]
        public UserDto User { get; set; }

        [JsonProperty("lastProject", Required = Required.AllowNull)]
        public ProjectDto LastProject { get; set; }

        [JsonProperty("lastProjectTasksAmount", Required = Required.Always)]
        public int LastProjectTasksAmount { get; set; }

        [JsonProperty("canceledOrNotDoneTasksAmount", Required = Required.Always)]
        public int CanceledOrNotDoneTasksAmount { get; set; }

        [JsonProperty("mostTimeConsumingTask", Required = Required.AllowNull)]
        public TaskDto MostTimeConsumingTask { get; set; }
    }
}
