﻿using Newtonsoft.Json;
using System;

namespace ProjectsManagementTool.Common.DTOs
{
    public class TeamDto
    {
        [JsonProperty("id", Required = Required.Default)]
        public int Id { get; set; }

        [JsonProperty("name", Required = Required.AllowNull)]
        public string Name { get; set; }

        [JsonProperty("createdAt", Required = Required.Default)]
        public DateTime CreatedAt { get; set; }
    }
}
