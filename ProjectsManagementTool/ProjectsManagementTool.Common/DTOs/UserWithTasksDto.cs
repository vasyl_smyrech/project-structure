﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace ProjectsManagementTool.Common.DTOs
{
    public class UserWithTasksDto
    {
        [JsonProperty("performer", Required = Required.Always)]
        public UserDto Performer { get; set; }

        [JsonProperty("tasks", Required = Required.Always)]
        public List<TaskDto> Tasks { get; set; }
    }
}
