﻿using Newtonsoft.Json;
using System;

namespace ProjectsManagementTool.Common.DTOs
{
    public class UserDto
    {
        [JsonProperty("id", Required = Required.Default)]
        public int Id { get; set; }

        [JsonProperty("firstName", Required = Required.Always)]
        public string FirstName { get; set; }

        [JsonProperty("lastName", Required = Required.Always)]
        public string LastName { get; set; }

        [JsonProperty("email", Required = Required.Always)]
        public string Email { get; set; }

        [JsonProperty("birthday", Required = Required.Always)]
        public DateTime Birthday { get; set; }

        [JsonProperty("registeredAt", Required = Required.Default)]
        public DateTime RegisteredAt { get; set; }

        [JsonProperty("teamId", Required = Required.AllowNull)]
        public int? TeamId { get; set; }
    }
}
