﻿using Newtonsoft.Json;

namespace ProjectsManagementTool.Common.DTOs
{
    public class ShortTaskInfoDto
    {
        [JsonProperty("taskId", Required = Required.Always)]
        public int TaskId { get; set; }

        [JsonProperty("taskName", Required = Required.Always)]
        public string TaskName { get; set; }
    }
}
