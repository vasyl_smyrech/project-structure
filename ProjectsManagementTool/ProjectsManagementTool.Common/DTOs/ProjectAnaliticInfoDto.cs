﻿using Newtonsoft.Json;

namespace ProjectsManagementTool.Common.DTOs
{
    public class ProjectAnaliticInfoDto
    {
        [JsonProperty("project", Required = Required.Always)]
        public ProjectDto Project { get; set; }

        [JsonProperty("taskWithLongestDescription", Required = Required.AllowNull)]
        public TaskDto TaskWithLongestDescription { get; set; }

        [JsonProperty("taskWithShortestName", Required = Required.AllowNull)]
        public TaskDto TaskWithShortestName { get; set; }

        [JsonProperty("membersCountByTasksCountAndProjDescLength", Required = Required.Always)]
        public int MembersCountByTasksCountAndProjDescLength { get; set; }
    }
}
