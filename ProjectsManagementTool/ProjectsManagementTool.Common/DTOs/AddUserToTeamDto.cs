﻿namespace ProjectsManagementTool.Common.DTOs
{
    public class AddUserToTeamDto
    {
        public int TeamId { get; set; }
        public UserDto NewMember { get; set; }
    }
}
