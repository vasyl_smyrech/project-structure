﻿using Newtonsoft.Json;
using ProjectsManagementTool.Common.Converters;
using System.Collections.Generic;

namespace ProjectsManagementTool.Common.DTOs
{
    public class ProjectTasksCountByProjectDto
    {
        [JsonConverter(typeof(DictionaryConverter<ProjectDto, int>))]
        [JsonProperty("tasksCountByProject", Required = Required.Always)]
        public Dictionary<ProjectDto, int> TasksCountByProject { get; set; }
    }
}
