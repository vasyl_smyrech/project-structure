﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace ProjectsManagementTool.Common.DTOs
{
    public class ProjectDto
    {
        [JsonProperty("id", Required = Required.Default)]
        public int Id { get; set; }

        [JsonProperty("name", Required = Required.AllowNull)]
        public string Name { get; set; }

        [JsonProperty("description", Required = Required.AllowNull)]
        public string Description { get; set; }

        [JsonProperty("createdAt", Required = Required.Default)]
        public DateTime CreatedAt { get; set; }

        [JsonProperty("deadline", Required = Required.Always)]
        public DateTime Deadline { get; set; }

        [JsonProperty("authorId", Required = Required.AllowNull)]
        public int? AuthorId { get; set; }

        [JsonProperty("author", Required = Required.Default)]
        public UserDto Author { get; set; }

        [JsonProperty("teamId", Required = Required.AllowNull)]
        public int TeamId { get; set; }

        [JsonProperty("team", Required = Required.Default)]
        public TeamDto Team { get; set; }

        [JsonProperty("tasks", Required = Required.Default)]
        public List<TaskDto> Tasks { get; set; }
    }
}
