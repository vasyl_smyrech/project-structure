﻿using Newtonsoft.Json;
using ProjectsManagementTool.Common.Models;
using System;

namespace ProjectsManagementTool.Common.DTOs
{
    public class TaskDto
    {
        [JsonProperty("id", Required = Required.Default)]
        public int Id { get; set; }

        [JsonProperty("name", Required = Required.AllowNull)]
        public string Name { get; set; }

        [JsonProperty("description", Required = Required.AllowNull)]
        public string Description { get; set; }

        [JsonProperty("createdAt", Required = Required.Default)]
        public DateTime CreatedAt { get; set; }

        [JsonProperty("finishedAt", Required = Required.Default)]
        public DateTime FinishedAt { get; set; }

        [JsonProperty("state", Required = Required.Always)]
        public TaskState State { get; set; }

        [JsonProperty("projectId", Required = Required.Always)]
        public int ProjectId { get; set; }

        [JsonProperty("performerId", Required = Required.Default)]
        public int PerformerId { get; set; }
    }
}
