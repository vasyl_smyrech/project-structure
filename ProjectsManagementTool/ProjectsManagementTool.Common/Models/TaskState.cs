﻿namespace ProjectsManagementTool.Common.Models
{
    public enum TaskState : byte
    {
        ToDo,
        InProgress,
        Done,
        Canceled
    }
}