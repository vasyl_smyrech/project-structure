﻿using CollectionsAndLinq.PmtConsole;
using CollectionsAndLinq.Services;
using Microsoft.Extensions.DependencyInjection;
using ProjectsManagementTool.Common.DTOs;
using ProjectsManagementTool.ConsoleApp.Helpers;
using ProjectsManagementTool.ConsoleApp.Interfaces;
using ProjectsManagementTool.ConsoleApp.PmtConsole;
using ProjectsManagementTool.ConsoleApp.Services;
using System;
using System.Threading.Tasks;

namespace CollectionsAndLinq
{
    public static class Program
    {
        private static IServiceProvider _serviceProvider;

        private static async Task Main(string[] _)
        {
            await RegisterServices();

            var console = _serviceProvider.GetRequiredService<IProjectManagementConsole>();
            await console.Start();

            await DisposeServices();
        }

        private async static Task RegisterServices()
        {
            var collection = new ServiceCollection()
                .AddSingleton<IHttpClientService, HttpClientService>()
                .AddSingleton<IOutputFormatter, OutputFormatter>()
                .AddSingleton<IInputFormatter, InputFormatter>()
                .AddSingleton<IProjectManagementConsole, ProjectManagementConsole>()
                .AddScoped<IAnaliticService, AnaliticService>()
                .AddScoped<IEntityService<UserDto>, EntityService<UserDto>>()
                .AddScoped<IEntityService<ProjectDto>, EntityService<ProjectDto>>()
                .AddScoped<IEntityService<TaskDto>, EntityService<TaskDto>>()
                .AddScoped<IEntityService<TeamDto>, EntityService<TeamDto>>()
                .AddScoped<ITaskService, TaskService>()
                .AddScoped<IMarkTaskService, MarkTaskService>()
                .AddScoped<IEntityMenu<UserDto>, EntityMenu<UserDto>>()
                .AddScoped<IEntityMenu<ProjectDto>, EntityMenu<ProjectDto>>()
                .AddScoped<IEntityMenu<TaskDto>, EntityMenu<TaskDto>>()
                .AddScoped<IEntityMenu<TeamDto>, EntityMenu<TeamDto>>()
                .AddScoped<ICreateEntityDialog<UserDto>, CreateEntityDialog<UserDto>>()
                .AddScoped<ICreateEntityDialog<ProjectDto>, CreateEntityDialog<ProjectDto>>()
                .AddScoped<ICreateEntityDialog<TaskDto>, CreateEntityDialog<TaskDto>>()
                .AddScoped<ICreateEntityDialog<TeamDto>, CreateEntityDialog<TeamDto>>()
                .AddScoped<IUpdateEntityMenu<UserDto>, UpdateEntityMenu<UserDto>>()
                .AddScoped<IUpdateEntityMenu<ProjectDto>, UpdateEntityMenu<ProjectDto>>()
                .AddScoped<IUpdateEntityMenu<TaskDto>, UpdateEntityMenu<TaskDto>>()
                .AddScoped<IUpdateEntityMenu<TeamDto>, UpdateEntityMenu<TeamDto>>()
                .AddScoped<IAnaliticMenu, AnaliticMenu>();

            _serviceProvider = collection.BuildServiceProvider();

            await Task.CompletedTask;
        }

        private static async Task DisposeServices()
        {
            if (_serviceProvider == null)
            {
                return;
            }
            if (_serviceProvider is IDisposable disposable)
            {
                disposable.Dispose();
            }

            await Task.CompletedTask;
        }
    }
}