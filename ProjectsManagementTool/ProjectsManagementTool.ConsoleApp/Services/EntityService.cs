﻿using ProjectsManagementTool.ConsoleApp.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectsManagementTool.ConsoleApp.Services
{
    public class EntityService<T> : IEntityService<T>
    {
        public EntityService(IHttpClientService httpService)
        {
            HttpService = httpService;
        }

        protected IHttpClientService HttpService { get; }
        protected string RequestUri { get; } = typeof(T).ToString().Split('.')[^1].ToLower()[0..^3] + 's';

        public async Task<T> Create(T entity)
            => await HttpService.PostAsync<T>(RequestUri, entity);

        public async Task Delete(int id)
            => await HttpService.DeleteAsync(RequestUri, id);

        public async Task<T> Read(int id)
            => await HttpService.GetAsync<T>($"{RequestUri}/{id}");

        public async Task<IList<T>> ReadAll()
            => await HttpService.GetListAsync<T>($"{RequestUri}");

        public async Task Update(T entity)
            => await HttpService.PutAsync(RequestUri, entity);
    }
}
