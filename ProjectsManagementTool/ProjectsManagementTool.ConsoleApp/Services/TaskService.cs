﻿using ProjectsManagementTool.Common.DTOs;
using ProjectsManagementTool.ConsoleApp.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectsManagementTool.ConsoleApp.Services
{
    public class TaskService : EntityService<TaskDto>, ITaskService
    {
        public TaskService(IHttpClientService httpClientService) : base(httpClientService) { }

        public async Task<List<TaskDto>> GetAllNotDoneTasks()
            => await HttpService.GetAsync<List<TaskDto>>($"{RequestUri}/allNotDoneTasks");

        public async Task MarkTaskAsDoneAsync(int taskId)
            => await HttpService.PutAsync<int>($"{RequestUri}/markTaskAsDone", taskId);
    }
}
