﻿using Newtonsoft.Json;
using ProjectsManagementTool.Common.Exceptions;
using ProjectsManagementTool.ConsoleApp.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsAndLinq.Services
{
    public class HttpClientService : IHttpClientService
    {
        private static readonly HttpClient _client = new HttpClient();

        static HttpClientService()
        {
            _client.BaseAddress = new Uri(ConfigurationManager.AppSettings.Get("api-url"));
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<T> GetAsync<T>(string requestUri)
        {
            var response = await _client.GetAsync(requestUri);
            if (!response.IsSuccessStatusCode)
                await ThrowException(response);

            var objectJsonString = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<T>(objectJsonString);
        }

        public async Task<List<T>> GetListAsync<T>(string requestUri)
        {
            var response = await _client.GetAsync(requestUri);
            if (!response.IsSuccessStatusCode)
                await ThrowException(response);

            var objectJsonString = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<List<T>>(objectJsonString);
        }

        public async Task<T> PostAsync<T>(string requestUri, T obj)
        {
            var response = await _client.PostAsync(requestUri,
                new StringContent(JsonConvert.SerializeObject(obj),
                Encoding.UTF8,
                "application/json"));

            if (!response.IsSuccessStatusCode)
                await ThrowException(response);

            var objectJsonString = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<T>(objectJsonString);
        }

        public async Task PutAsync<T>(string requestUri, T obj)
        {
            var response = await _client.PutAsync(requestUri,
                new StringContent(JsonConvert.SerializeObject(obj),
                Encoding.UTF8,
                "application/json"));

            if (!response.IsSuccessStatusCode)
                await ThrowException(response);
        }

        public async Task DeleteAsync(string requestUri, int id)
        {
            var response = await _client.DeleteAsync($"{requestUri}/{id}");

            if (!response.IsSuccessStatusCode)
                await ThrowException(response);
        }

        private async Task ThrowException(HttpResponseMessage response)
        {
            if (response.StatusCode == HttpStatusCode.NotFound)
                throw new NotFoundException(await response.Content.ReadAsStringAsync(), false);
            throw new Exception($"Response code - {response.StatusCode}" +
          $"\n\t{await response.Content.ReadAsStringAsync()}");
        }
    }
}
