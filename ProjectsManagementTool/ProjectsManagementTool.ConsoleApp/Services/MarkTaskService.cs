﻿using ProjectsManagementTool.Common.Exceptions;
using ProjectsManagementTool.ConsoleApp.Interfaces;
using System;
using System.Threading.Tasks;

namespace ProjectsManagementTool.ConsoleApp.Services
{
    public class MarkTaskService : IMarkTaskService
    {
        private readonly ITaskService _taskService;

        public MarkTaskService(ITaskService taskService)
        {
            _taskService = taskService;
        }

        public async Task<int> MarkRandomTaskWithDelay(int delayInMs)
        {
            var tcs = new TaskCompletionSource<int>();

            var timer = new System.Timers.Timer(delayInMs);
            timer.Elapsed += async (obj, args) =>
            {
                int result;

                try
                {
                    result = await MarkRandomTask();
                    tcs.SetResult(result);
                }
                catch (Exception ex)
                {
                    tcs.SetException(ex);
                }
            };
            timer.AutoReset = false;
            timer.Enabled = true;

            return await tcs.Task;
        }

        private async Task<int> MarkRandomTask()
        {
            var tasks = await _taskService.GetAllNotDoneTasks();

            if (tasks.Count == 0)
                throw new NotFoundException("There is no any not done task", false);

            var shouldMarkAsDoneTaskId = tasks[new Random().Next(tasks.Count - 1)].Id;

            await _taskService.MarkTaskAsDoneAsync(shouldMarkAsDoneTaskId);

            return await Task.FromResult(shouldMarkAsDoneTaskId);
        }
    }
}
