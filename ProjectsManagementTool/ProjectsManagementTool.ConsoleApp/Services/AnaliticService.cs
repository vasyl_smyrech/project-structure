﻿using ProjectsManagementTool.Common.DTOs;
using ProjectsManagementTool.ConsoleApp.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectsManagementTool.ConsoleApp.Services
{
    public class AnaliticService : IAnaliticService
    {
        private readonly IHttpClientService _httpService;
        private const string requestUri = "analitic";

        public AnaliticService(IHttpClientService httpService)
        {
            _httpService = httpService;
        }

        public async Task<ProjectTasksCountByProjectDto> GetProjectTasksCountByAuthorId(int authorId)
            => await _httpService.GetAsync<ProjectTasksCountByProjectDto>($"{requestUri}/projectTasksCountByProjectAuthorId/{authorId}");

        public async Task<List<TaskDto>> GetPerformerTasksByNameLength(int performerId)
            => await _httpService.GetAsync<List<TaskDto>>($"{requestUri}/performerTasksByNameLength/{performerId}");

        public async Task<List<ShortTaskInfoDto>> GetPerformerFinishedTasksByYear(int performerId)
            => await _httpService.GetAsync<List<ShortTaskInfoDto>>($"{requestUri}/performerFinishedTasksByYear/{performerId}");

        public async Task<List<TeamWithMembersDto>> GetTeamsAndMembersFilteredByMembersAge()
            => await _httpService.GetAsync<List<TeamWithMembersDto>>($"{requestUri}/teamsAndMembersFilteredByMembersAge");

        public async Task<List<UserWithTasksDto>> GetUsersByFirstNameWithTasksSortedByNameLength()
            => await _httpService.GetAsync<List<UserWithTasksDto>>($"{requestUri}/usersByFirstNameWithTasksSortedByNameLength");

        public async Task<UserAnaliticInfoDto> GetUserAnaliticInfo(int userId)
            => await _httpService.GetAsync<UserAnaliticInfoDto>($"{requestUri}/userAnaliticInfo/{userId}");

        public async Task<List<ProjectAnaliticInfoDto>> GetProjectsAnaliticInfo()
            => await _httpService.GetAsync<List<ProjectAnaliticInfoDto>>($"{requestUri}/projectsAnaliticInfo");
    }
}
