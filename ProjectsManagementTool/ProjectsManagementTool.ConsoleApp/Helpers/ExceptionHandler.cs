﻿using ProjectsManagementTool.Common.Exceptions;
using ProjectsManagementTool.ConsoleApp.Interfaces;
using System;
using System.Threading.Tasks;

namespace ProjectsManagementTool.ConsoleApp.Helpers
{
    public static class ExceptionHandler
    {
        public static async Task HandleAsync(Func<Task> processed, Func<Task> callback,
            IOutputFormatter outputFormatter, string commonErrorMessage = null)
        {
            try
            {
                await processed?.Invoke();
            }
            catch (AggregateException)
            {
                outputFormatter.Error("No connection to the server");
            }
            catch (NotFoundException ex)
            {
                outputFormatter.Info(ex.Message);
            }
            catch (Exception ex)
            {
                outputFormatter.Info(commonErrorMessage ?? ex.Message);
            }
            finally
            {
                outputFormatter.Quitable();
                await callback?.Invoke();
            }
        }
    }
}
