﻿using ProjectsManagementTool.Common.DTOs;

namespace ProjectsManagementTool.ConsoleApp.Helpers
{
    public static class OutputMessageHalper
    {
        public static string EntityInfo<T>(T entity)
        {
            if (entity is UserDto user)
            {
                return "User: " +
                    $"\n\t\tid - {user.Id}" +
                    $"\n\t\tname - {user.FirstName} {user.LastName}" +
                    $"\n\t\temail - {user.Email}" +
                    $"\n\t\tbirthday - {user.Birthday:MM/dd/yyyy}" +
                    $"\n\t\tregistered at {user.RegisteredAt}";
            }

            if (entity is ProjectDto project)
            {
                return "Project" +
                    $"\n\t\tid - {project.Id}" +
                    $"\n\t\tname - {project.Name}" +
                    $"\n\t\tdescription - {project.Description}" +
                    $"\n\t\tauthor id - {project.AuthorId}" +
                    $"\n\t\tteam id - {project.TeamId}" +
                    $"\n\t\tcreated at {project.CreatedAt}" +
                    $"\n\t\tdeadline - {project.Deadline}";
            }

            if (entity is TaskDto task)
            {
                return "Task" +
                    $"\n\t\tid - {task.Id}" +
                    $"\n\t\tname - {task.Name}" +
                    $"\n\t\tdescription - {task.Description}" +
                    $"\n\t\tperformer id - {task.PerformerId}" +
                    $"\n\t\tproject id - {task.ProjectId}" +
                    $"\n\t\tcreated at {task.CreatedAt}" +
                    $"\n\t\tfinished at {task.FinishedAt}";
            }

            if (entity is TeamDto team)
            {
                return "Team" +
                    $"\n\t\tid - {team.Id}" +
                    $"\n\t\tname - {team.Name}" +
                    $"\n\t\tcreated at {team.CreatedAt}";
            }

            return string.Empty;
        }
    }
}
