﻿using ProjectsManagementTool.ConsoleApp.Interfaces;
using System;
using System.Drawing;
using System.Threading.Tasks;

namespace ProjectsManagementTool.ConsoleApp.Helpers
{
    public class InputFormatter : IInputFormatter
    {
        private readonly IOutputFormatter _outputFormatter;

        public InputFormatter(IOutputFormatter outputFormatter)
        {
            _outputFormatter = outputFormatter;
        }

        public async Task<int?> InputPositiveInteger(string beforeInputText)
        {
            var wasIdParsed = false;
            var id = 0;

            while (!wasIdParsed || id < 0)
            {
                wasIdParsed = Int32.TryParse(await InputText(beforeInputText), out id);
                if (!wasIdParsed || id < 0)
                {
                    _outputFormatter.CenteredText("You have entered invalid number.", Color.Red);
                    _outputFormatter.Quitable();
                }
            }

            return wasIdParsed ? (int?)id : null;
        }

        public Task<string> InputText(string beforeInputText)
        {
            Console.Write($"\n\t{beforeInputText}", Color.Bisque);
            return Task.FromResult<string>(Console.ReadLine());
        }
    }
}
