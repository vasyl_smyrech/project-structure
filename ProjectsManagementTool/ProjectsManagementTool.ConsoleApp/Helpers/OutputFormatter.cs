﻿using ProjectsManagementTool.ConsoleApp.Interfaces;
using System;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Console = Colorful.Console;

namespace ProjectsManagementTool.ConsoleApp.Helpers
{
    public class OutputFormatter : IOutputFormatter
    {
        public void ClearConsole()
        {
            Console.Clear();
            CenteredText("Project Management Tool");
        }

        public void CenteredText(string text)
        {
            CenteredText($"{text}\t", Color.Aqua);
        }

        public void CenteredText(string text, Color color)
        {
            var offset = string.Empty;
            var startTextPosition = (Console.WindowWidth - text.Length) / 2;
            for (var i = 0; i < startTextPosition; i++)
            {
                offset += " ";
            }
            Console.Write($"\n{offset}{text}\n\t", color);
        }

        public void Error(string errorMessage)
        {
            Error(errorMessage, null);
        }

        public void Error(string errorMessage, Action fallback)
        {
            Console.WriteLine($"\n\t{errorMessage}\n\tPress any key to proceed\n\t", Color.Red);
            Console.ReadKey();
            fallback?.Invoke();
        }

        public void Info(string info)
        {
            Console.Write($"\n\n\t{info}\n\t", Color.Aquamarine);
        }

        public void ListItem(string itemText)
        {
            Console.Write($"\n\t{itemText}", Color.Aquamarine);
        }

        public async Task List(Func<Task> fallback, params (string, Func<Task>)[] items)
        {
            var formatted = string.Empty;
            var additionalWitespaceIfmoreThenNineItems = items.Length > 9 ? " " : string.Empty;

            for (var i = 0; i < items.Length; i++)
            {
                formatted += $"\n\t{i + 1}{additionalWitespaceIfmoreThenNineItems} - {items[i].Item1}";
            }

            Console.Write($"{formatted}\n\t", Color.Bisque);
            if (items.Length < 10)
            {
                var answer = Console.ReadKey();
                var listLength = items.Length.ToString();
                Regex regex = new Regex($"[1-{listLength}]");

                if (!regex.IsMatch(answer.KeyChar.ToString()))
                {
                    ClearConsole();
                    await fallback.Invoke();
                }

                try
                {
                    await items[Int32.Parse(answer.KeyChar.ToString()) - 1].Item2.Invoke();
                }
                catch (Exception)
                {
                    await fallback?.Invoke();
                }
            }
        }

        public void Quitable()
        {
            Info("Press any key to proceed");
            Console.ReadKey();
        }
    }
}
