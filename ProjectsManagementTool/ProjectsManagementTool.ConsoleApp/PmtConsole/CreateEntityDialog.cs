﻿using ProjectsManagementTool.Common.DTOs;
using ProjectsManagementTool.ConsoleApp.Interfaces;
using System;
using System.Threading.Tasks;

namespace ProjectsManagementTool.ConsoleApp.PmtConsole
{
    public class CreateEntityDialog<T> : ICreateEntityDialog<T> where T : new()
    {
        private readonly string tTypeName = typeof(T).ToString().Split('.')[^1][0..^3];
        private readonly IOutputFormatter _outputFormatter;
        private readonly IInputFormatter _inputFormatter;
        private readonly IEntityService<T> _entityService;
        private readonly T _entity = new T();

        public CreateEntityDialog(
           IOutputFormatter outputFormatter,
           IInputFormatter inputFormatter,
           IEntityService<T> entityService)
        {
            _outputFormatter = outputFormatter;
            _inputFormatter = inputFormatter;
            _entityService = entityService;
        }

        public async Task ShowDialogAsync()
        {
            _outputFormatter.ClearConsole();
            _outputFormatter.CenteredText($"{tTypeName} creating");

            if (_entity is UserDto user)
            {
                user.FirstName = await _inputFormatter.InputText("Enter new user first name: ");
                user.LastName = await _inputFormatter.InputText("Enter new user last name: ");
                user.Email = await _inputFormatter.InputText("Enter new user email: ");
                var year = (int) await _inputFormatter.InputPositiveInteger("Enter the year of birth: ");
                var month = (int) await _inputFormatter.InputPositiveInteger("Enter the month of birth: ");
                var day = (int) await _inputFormatter.InputPositiveInteger("Enter the day number of birth: ");
                try
                {
                    user.Birthday = new DateTime(year, month, day);
                }
                catch (Exception)
                {
                    user.Birthday = DateTime.Now.AddYears(-20);
                }
                user.Birthday = new System.DateTime(year, month, day);
                user.TeamId = (int) await _inputFormatter.InputPositiveInteger("Enter a new user team id: ");
            }

            if (_entity is ProjectDto project)
            {
                project.Name = await _inputFormatter.InputText("Enter new project name: ");
                project.Description = await _inputFormatter.InputText("Enter new project description: ");
                project.AuthorId = (int) await _inputFormatter.InputPositiveInteger("Enter a new project author id: ");
                project.TeamId = (int) await _inputFormatter.InputPositiveInteger("Enter a new project team id: ");
                var year = (int) await _inputFormatter.InputPositiveInteger("Enter the year of deadline: ");
                var month = (int) await _inputFormatter.InputPositiveInteger("Enter the month of deadline: ");
                var day = (int) await _inputFormatter.InputPositiveInteger("Enter the day number of deadline: ");
                try
                {
                    project.Deadline = new DateTime(year, month, day);
                }
                catch (Exception)
                {
                    project.Deadline = DateTime.Now.AddMonths(5);
                }
            }

            if (_entity is TaskDto task)
            {
                task.Name = await _inputFormatter.InputText("Enter new task name: ");
                task.Description = await _inputFormatter.InputText("Enter new task description: ");
                task.ProjectId = (int) await _inputFormatter.InputPositiveInteger("Enter a new task project id: ");
                task.PerformerId = (int) await _inputFormatter.InputPositiveInteger("Enter a new task performer id: ");
                task.State = 0;
            }

            if (_entity is TeamDto team)
            {
                team.Name = await _inputFormatter.InputText("Enter new team name: ");
            }

            await CreateEntity();
        }

        private async Task CreateEntity()
        {
            try
            {
                var newEntity = await _entityService.Create(_entity);
                _outputFormatter.Info($"New {tTypeName.ToLower()} successfuly created");
            }
            catch (Exception)
            {
                _outputFormatter.Info($"Error duering {tTypeName} creating");
            }
            finally
            {
                _outputFormatter.Quitable();
            }
        }
    }
}
