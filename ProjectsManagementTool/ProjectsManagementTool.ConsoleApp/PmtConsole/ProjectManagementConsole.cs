﻿using ProjectsManagementTool.Common.DTOs;
using ProjectsManagementTool.Common.Exceptions;
using ProjectsManagementTool.ConsoleApp.Interfaces;
using System;
using System.Threading.Tasks;

namespace CollectionsAndLinq.PmtConsole
{
    public class ProjectManagementConsole : IProjectManagementConsole
    {
        private readonly IOutputFormatter _outputFormatter;
        private readonly IEntityMenu<UserDto> _userMenu;
        private readonly IEntityMenu<ProjectDto> _projectMenu;
        private readonly IEntityMenu<TaskDto> _taskMenu;
        private readonly IEntityMenu<TeamDto> _teamMenu;
        private readonly IAnaliticMenu _analiticMenu;
        private readonly IMarkTaskService _markTaskService;
        private string _message;

        public ProjectManagementConsole(
            IOutputFormatter outputFormatter,
            IEntityMenu<UserDto> userMenu,
            IEntityMenu<ProjectDto> projectMenu,
            IEntityMenu<TaskDto> taskMenu,
            IEntityMenu<TeamDto> teamMenu,
            IAnaliticMenu analiticMenu,
            IMarkTaskService markTaskService
            )
        {
            _outputFormatter = outputFormatter;
            _userMenu = userMenu;
            _projectMenu = projectMenu;
            _taskMenu = taskMenu;
            _teamMenu = teamMenu;
            _analiticMenu = analiticMenu;
            _markTaskService = markTaskService;
        }

        public async Task Start()
        {
            await ShowMainMenuAsync();
        }

        private async Task ShowMainMenuAsync()
        {
            _outputFormatter.ClearConsole();

            if (!string.IsNullOrEmpty(_message))
            {
                _outputFormatter.Info(_message);
                _message = string.Empty;
            }

            _outputFormatter.CenteredText("Main menu");
            _outputFormatter.CenteredText("Press digit for item chosing:");
            await _outputFormatter.List(ShowMainMenuAsync,
                ("User", async () => await _userMenu.ShowMenuAsync(ShowMainMenuAsync)),
                ("Project", async () => await _projectMenu.ShowMenuAsync(ShowMainMenuAsync)),
                ("Task", async () => await _taskMenu.ShowMenuAsync(ShowMainMenuAsync)),
                ("Team", async () => await _teamMenu.ShowMenuAsync(ShowMainMenuAsync)),
                ("Analitic information", async () => await _analiticMenu.ShowMenuAsync(ShowMainMenuAsync)),
                ("Mark random task as done", MarkRandomTaskAsDone));

            await Task.CompletedTask;
        }

        private async Task MarkRandomTaskAsDone()
        {
            _ = Task.Run(async () => await ShowMarkRandomTaskResult());
            _message = "The task is running in background";
            await ShowMainMenuAsync();
        }

        private async Task ShowMarkRandomTaskResult()
        {
            string resultMessage = string.Empty;
            string errorMessage = string.Empty;

            try
            {
                var result = await _markTaskService.MarkRandomTaskWithDelay(1000);
                resultMessage = $"Task with id {result} was successfuly marked as done";
            }
            catch (NotFoundException)
            {
                resultMessage = "There is no any not done task";
            }
            catch (Exception)
            {
                errorMessage = "Error during marking random task as done";
            }
            finally
            {
                if (string.IsNullOrEmpty(resultMessage))
                    _outputFormatter.Error(errorMessage);
                else
                    _outputFormatter.Info(resultMessage);
            }
        }
    }
}
