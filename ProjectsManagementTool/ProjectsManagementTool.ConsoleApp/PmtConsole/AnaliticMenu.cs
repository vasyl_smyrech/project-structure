﻿using ProjectsManagementTool.Common.DTOs;
using ProjectsManagementTool.ConsoleApp.Helpers;
using ProjectsManagementTool.ConsoleApp.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectsManagementTool.ConsoleApp.PmtConsole
{
    internal class AnaliticMenu : IAnaliticMenu
    {
        private readonly IOutputFormatter _outputFormatter;
        private readonly IInputFormatter _inputFormatter;
        private readonly IAnaliticService _analiticService;
        private Func<Task> _callback;

        public AnaliticMenu(
            IOutputFormatter outputFormatter,
            IInputFormatter inputFormatter,
            IAnaliticService analiticService)
        {
            _outputFormatter = outputFormatter;
            _inputFormatter = inputFormatter;
            _analiticService = analiticService;
        }

        public async Task ShowMenuAsync(Func<Task> callback)
        {
            _callback ??= callback;

            _outputFormatter.ClearConsole();
            _outputFormatter.CenteredText("Press digit for item chosing:");
            await _outputFormatter.List(() => ShowMenuAsync(callback),
                ("Show tasks amount in each project by an user id", async () => await ShowTaskAmountInProjectDialog()),
                ("Show tasks where a task name length less than 45 by an user id", async () => await ShowTaskByTaskNameLengthDialog()),
                ("Show finished in 2020 tasks by an user id", async () => await ShowUsersFinishedTasksDialog()),
                ("Show teams with members where members more than 10 years old", async () => await ShowTeamsWithMembers()),
                ("Show sorted alphabetically by name users list with descended sorted tasks by name length lists",
                    async () => await ShowUsersWithTasks()),
                ("Show an user's analitics data by id", async () => await ShowUsersAnaliticsData()),
                ("Show projects' analitics data", async () => await ShowProjectsAnaliticsData()),
                ("Main menu", () => callback.Invoke()));

            await Task.CompletedTask;
        }

        private async Task ShowMenuAsync()
        {
            await ShowMenuAsync(_callback);
        }

        private async Task ShowTaskAmountInProjectDialog()
        {
            _outputFormatter.ClearConsole();
            _outputFormatter.CenteredText("Show tasks amount in each project by an author id");

            var userId = await _inputFormatter.InputPositiveInteger("Enter an author id: ");
            ProjectTasksCountByProjectDto result;

            await ExceptionHandler.HandleAsync(
                async () => {
                    result = await _analiticService.GetProjectTasksCountByAuthorId(userId.Value);

                    if (result.TasksCountByProject.Count == 0)
                    {
                        _outputFormatter.Info($"User with id {userId} have no any projects");
                    }
                    else
                    {
                        foreach (var item in result.TasksCountByProject)
                        {
                            _outputFormatter.ListItem($"{item.Key.Name} - {item.Value}");
                        }
                    }
                },
                ShowMenuAsync,
                _outputFormatter,
                "Error during query execution"
            );
        }

        private async Task ShowTaskByTaskNameLengthDialog()
        {
            _outputFormatter.ClearConsole();
            _outputFormatter.CenteredText("Show tasks where a task name length less than 45 by an user id");

            var userId = await _inputFormatter.InputPositiveInteger("Enter a performer id: ");

            List<TaskDto> result;

            await ExceptionHandler.HandleAsync(
                async () => {
                    result = await _analiticService.GetPerformerTasksByNameLength(userId.Value);

                    if (result.Count == 0)
                    {
                        _outputFormatter.Info($"User with id {userId} have no any matching tasks");
                    }
                    else
                    {
                        foreach (var item in result)
                        {
                            _outputFormatter.ListItem($"Task Id: {item.Id}\n\tTask name: {item.Name}\n\tPerformer Id {item.PerformerId}\n");
                        }
                    }
                },
                ShowMenuAsync,
                _outputFormatter,
                "Error during query execution"
            );
        }

        private async Task ShowUsersFinishedTasksDialog()
        {
            _outputFormatter.ClearConsole();
            _outputFormatter.CenteredText("Show finished in 2020 tasks by an userid");

            var userId = await _inputFormatter.InputPositiveInteger("Enter a performer id: ");

            List<ShortTaskInfoDto> result;

            await ExceptionHandler.HandleAsync(
                async () => {
                    result = await _analiticService.GetPerformerFinishedTasksByYear(userId.Value);

                    if (result.Count == 0)
                    {
                        _outputFormatter.Info($"User with id {userId} have no any matching tasks");
                    }
                    else
                    {
                        foreach (var item in result)
                        {
                            _outputFormatter.ListItem($"Task id: {item.TaskId}\n\tTask name: {item.TaskName}\n");
                        }
                    }
                },
                ShowMenuAsync,
                _outputFormatter,
                "Error during query execution"
            );
        }

        private async Task ShowTeamsWithMembers()
        {
            _outputFormatter.ClearConsole();
            _outputFormatter.CenteredText("Show teams with members where members more than 10 years old");

            List<TeamWithMembersDto> result;

            await ExceptionHandler.HandleAsync(
                async () => {
                    result = await _analiticService.GetTeamsAndMembersFilteredByMembersAge();

                    if (result.Count == 0)
                    {
                        _outputFormatter.Info("There is no any teams");
                    }
                    else
                    {
                        foreach (var item in result)
                        {
                            _outputFormatter.ListItem($"Team id: {item.TeamId}\n\tTeam name: {item.TeamName}\n");

                            foreach (var user in item.Members)
                            {
                                _outputFormatter.ListItem($"\tUser id: {user.Id} - User name: {user.FirstName} {user.LastName}\n");
                            }
                        }
                    }
                },
                ShowMenuAsync,
                _outputFormatter,
                "Error during query execution"
            );
        }

        private async Task ShowUsersWithTasks()
        {
            _outputFormatter.ClearConsole();
            _outputFormatter.CenteredText("Show sorted alphabetically by name users list with descended sorted tasks by name length lists");

            List<UserWithTasksDto> result;

            await ExceptionHandler.HandleAsync(
                async () => {
                    result = await _analiticService.GetUsersByFirstNameWithTasksSortedByNameLength();

                    foreach (var item in result)
                    {
                        _outputFormatter.ListItem($"User id: {item.Performer.Id} - User name: {item.Performer.FirstName} {item.Performer.LastName}\n");

                        foreach (var task in item.Tasks)
                        {
                            _outputFormatter.ListItem($"\tTask id: {task.Id} - Task name: {task.Name}\n");
                        }
                    }
                },
                ShowMenuAsync,
                _outputFormatter,
                "Error during query execution"
           );
        }

        private async Task ShowUsersAnaliticsData()
        {
            _outputFormatter.ClearConsole();
            _outputFormatter.CenteredText("Show an user's analitics data by id");
            var userId = await _inputFormatter.InputPositiveInteger("Enter an user id: ");

            UserAnaliticInfoDto result;

            await ExceptionHandler.HandleAsync(
                async () => {
                    result = await _analiticService.GetUserAnaliticInfo(userId.Value);

                    if (result == null)
                    {
                        _outputFormatter.Info($"There is no user with id - {userId}");
                    }
                    else
                    {
                        _outputFormatter.ListItem($"User Id: {result.User.Id} - User name: {result.User.FirstName} {result.User.LastName}");

                        if (result.LastProject != null)
                        {
                            _outputFormatter.ListItem($"Last project id: {result.LastProject.Id} - Last project name: {result.LastProject.Name}\n\t" +
                            $"Last project tasks count: {result.LastProjectTasksAmount}\n\t" +
                            $"User canceled or not finished task count: {result.CanceledOrNotDoneTasksAmount}\n\t" +
                            $"The most time consuming task id-name: {result.MostTimeConsumingTask.Id} - {result.MostTimeConsumingTask.Name}\n\t");
                        }
                    }
                },
                ShowMenuAsync,
                _outputFormatter,
                "Error during query execution"
            );
        }

        private async Task ShowProjectsAnaliticsData()
        {
            _outputFormatter.ClearConsole();
            _outputFormatter.CenteredText("Show projects' analitics data");

            List<ProjectAnaliticInfoDto> result;

            await ExceptionHandler.HandleAsync(
                async () =>
                {
                    result = await _analiticService.GetProjectsAnaliticInfo();

                    foreach (var item in result)
                    {
                        _outputFormatter.ListItem($"Project Id: {item.Project.Id} - Project name: {item.Project.Name}\n\t" +
                            $"Project tasks count in projects with description length more than 20 characters \n\t\tor tasks amount less than 3: {item.MembersCountByTasksCountAndProjDescLength}");

                        if (item.Project.Tasks.Count > 0)
                        {
                            _outputFormatter.ListItem($"Task with the shortest name Id - name: {item.TaskWithShortestName.Id} - {item.TaskWithShortestName.Name}\n\t" +
                                                $"Task with the longest description Id - name: {item.TaskWithLongestDescription.Id} - {item.TaskWithLongestDescription.Name}\n\t");
                        }
                    }
                },
                ShowMenuAsync,
                _outputFormatter,
                "Error during query execution"
            );
        }
    }
}
