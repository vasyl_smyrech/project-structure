﻿using ProjectsManagementTool.Common.Exceptions;
using ProjectsManagementTool.ConsoleApp.Helpers;
using ProjectsManagementTool.ConsoleApp.Interfaces;
using System;
using System.Threading.Tasks;

namespace ProjectsManagementTool.ConsoleApp.PmtConsole
{
    public class EntityMenu<T> : IEntityMenu<T>
    {
        private readonly IOutputFormatter _outputFormatter;
        private readonly IInputFormatter _inputFormatter;
        private readonly IEntityService<T> _entityService;
        private readonly ICreateEntityDialog<T> _createEntityDialog;
        private readonly IUpdateEntityMenu<T> _updateEntityMenu;
        private Func<Task> _callback;
        private readonly string tTypeName = typeof(T).ToString().Split('.')[^1][0..^3];

        public EntityMenu(
           IOutputFormatter outputFormatter,
           IInputFormatter inputFormatter,
           IEntityService<T> entityService,
           ICreateEntityDialog<T> createEntityDialog,
           IUpdateEntityMenu<T> updateEntityMenu)
        {
            _outputFormatter = outputFormatter;
            _inputFormatter = inputFormatter;
            _entityService = entityService;
            _createEntityDialog = createEntityDialog;
            _updateEntityMenu = updateEntityMenu;
        }

        public async Task ShowMenuAsync(Func<Task> callback)
        {
            _callback ??= callback;

            _outputFormatter.ClearConsole();
            _outputFormatter.CenteredText("Press digit for item chosing:");
            await _outputFormatter.List(() => ShowMenuAsync(callback),
                ($"Show all {tTypeName.ToLower()}s", async () => await ShowAllEntitiesAsync()),
                ($"Show {tTypeName.ToLower()} by id", async () => await ShowEntityByIdAsync()),
                ($"Create {tTypeName.ToLower()}", async () => await CreateEntityAsync()),
                ($"Update {tTypeName.ToLower()}", async () => await UpdateEntityAsync()),
                ($"Delete {tTypeName.ToLower()}", async () => await DeleteEntityAsync()),
                ("Main menu", () => callback.Invoke()));

            await Task.CompletedTask;
        }

        private async Task ShowMenuAsync()
        {
            await ShowMenuAsync(_callback);
        }

        private async Task ShowAllEntitiesAsync()
        {
            _outputFormatter.ClearConsole();
            _outputFormatter.CenteredText($"All {tTypeName.ToLower()}s");

            try
            {
                var result = await _entityService.ReadAll();

                foreach (var item in result)
                {
                    _outputFormatter.ListItem(OutputMessageHalper.EntityInfo<T>(item));
                }

                _outputFormatter.Quitable();
                await ShowMenuAsync();
            }
            catch (AggregateException)
            {
                _outputFormatter.Error("No connection to the server");
                await ShowMenuAsync();
                return;
            }
        }

        private async Task ShowEntityByIdAsync()
        {
            _outputFormatter.ClearConsole();
            _outputFormatter.CenteredText($"{tTypeName} by id");

            var entityId = await _inputFormatter.InputPositiveInteger($"Enter the {tTypeName} id: ");

            try
            {
                var result = await _entityService.Read(entityId.Value);
                _outputFormatter.ListItem(OutputMessageHalper.EntityInfo<T>(result));

                _outputFormatter.Quitable();
            }
            catch (AggregateException)
            {
                _outputFormatter.Error("No connection to the server");
            }
            catch (NotFoundException ex)
            {
                _outputFormatter.Error(ex.Message);
            }
            finally
            {
                _outputFormatter.Quitable();
                await ShowMenuAsync();
            }
        }

        private async Task CreateEntityAsync()
        {
            await _createEntityDialog.ShowDialogAsync();

            await ShowMenuAsync();
        }

        private async Task UpdateEntityAsync()
        {
            await _updateEntityMenu.ShowMenuAsync();

            await ShowMenuAsync();
        }

        private async Task DeleteEntityAsync()
        {
            _outputFormatter.ClearConsole();
            _outputFormatter.CenteredText($"Delete {tTypeName.ToLower()}");

            var entityId = await _inputFormatter.InputPositiveInteger($"Enter the {tTypeName} id: ");

            try
            {
                await _entityService.Delete(entityId.Value);
            }
            catch (AggregateException)
            {
                _outputFormatter.Error("No connection to the server");
                _outputFormatter.Quitable();
                await ShowMenuAsync();
                return;
            }
            catch (Exception ex)
            {
                _outputFormatter.Error(ex.Message);
                _outputFormatter.Quitable();
                await ShowMenuAsync();
                return;
            }

            _outputFormatter.Info($"{tTypeName} with id {entityId} was successfuly deleted");
            _outputFormatter.Quitable();
            await ShowMenuAsync();
        }
    }
}
