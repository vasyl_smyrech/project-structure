﻿using ProjectsManagementTool.Common.DTOs;
using ProjectsManagementTool.ConsoleApp.Helpers;
using ProjectsManagementTool.ConsoleApp.Interfaces;
using System;
using System.Threading.Tasks;

namespace ProjectsManagementTool.ConsoleApp.PmtConsole
{
    public class UpdateEntityMenu<T> : IUpdateEntityMenu<T> where T : class
    {
        private readonly string tTypeName = typeof(T).ToString().Split('.')[^1][0..^3];
        private readonly IOutputFormatter _outputFormatter;
        private readonly IInputFormatter _inputFormatter;
        private readonly IEntityService<T> _entityService;

        public UpdateEntityMenu(
           IOutputFormatter outputFormatter,
           IInputFormatter inputFormatter,
           IEntityService<T> entityService)
        {
            _outputFormatter = outputFormatter;
            _inputFormatter = inputFormatter;
            _entityService = entityService;
        }

        public async Task ShowMenuAsync()
        {
            _outputFormatter.ClearConsole();
            _outputFormatter.CenteredText($"{tTypeName} updating");

             var entityId = (int) await _inputFormatter.InputPositiveInteger($"Enter an updating {tTypeName.ToLower()} id: ");

            T entity;

            try
            {
                entity = await _entityService.Read(entityId);
                _outputFormatter.Info(OutputMessageHalper.EntityInfo<T>(entity));
            }
            catch (AggregateException)
            {
                _outputFormatter.Error("No connection to the server");
                _outputFormatter.Quitable();
                return;
            }
            catch (Exception ex)
            {
                _outputFormatter.Error(ex.Message);
                return;
            }

            _outputFormatter.CenteredText("Press digit for item chosing:");
            var shouldUpdate = true;

            if (entity is UserDto user)
            {
                await _outputFormatter.List(async () => await ShowMenuAsync(),
                    ($"Update {tTypeName.ToLower()} first name",
                    async () => user.FirstName = await _inputFormatter.InputText("Enter updating user`s first name: ")),
                    ($"Update {tTypeName.ToLower()} last name",
                    async () => user.LastName = await _inputFormatter.InputText("Enter updating user`s last name: ")),
                    ($"Update {tTypeName.ToLower()} email",
                    async () => user.Email = await _inputFormatter.InputText("Enter updating user`s email: ")
                ),
                    ("Quit",async () => { shouldUpdate = false; await Task.CompletedTask; }));
            }

            if (entity is ProjectDto project)
            {
                await _outputFormatter.List(async () => await ShowMenuAsync(),
                     ($"Update {tTypeName.ToLower()} name",
                     async () => project.Name = await _inputFormatter.InputText("Enter updating project`s name: ")
                ),
                     ($"Update {tTypeName.ToLower()} description",
                     async () => project.Description = await _inputFormatter.InputText("Enter updating project`s description: ")
                ),
                     ($"Update {tTypeName.ToLower()} team id",
                     async () => project.TeamId = (int)await _inputFormatter.InputPositiveInteger("Enter updating project`s team id: ")
                ),
                     ("Quit", async () => { shouldUpdate = false; await Task.CompletedTask; }
                ));
            }

            if (entity is TaskDto task)
            {
                await _outputFormatter.List(async () => await ShowMenuAsync(),
                    ($"Update {tTypeName.ToLower()} name",
                    async () => task.Name = await _inputFormatter.InputText("Enter updating task`s name: ")
                ),
                    ($"Update {tTypeName.ToLower()} description",
                    async () => task.Description = await _inputFormatter.InputText("Enter updating task`s description: ")
                ),
                    ($"Update {tTypeName.ToLower()} project id",
                    async () => task.ProjectId = (int)await _inputFormatter.InputPositiveInteger("Enter updating task`s project id: ")
                ),
                       ($"Update {tTypeName.ToLower()} performer id",
                    async () => task.PerformerId = (int)await _inputFormatter.InputPositiveInteger("Enter updating task`s performer id: ")
                ),
                      ($"Mark {tTypeName.ToLower()} task as finished just now",
                    async () => { task.FinishedAt = DateTime.Now; await Task.CompletedTask; }
                ),
                    ("Quit", async () => { shouldUpdate = false; await Task.CompletedTask; }
                ));
            }

            if (entity is TeamDto team)
            {
                await _outputFormatter.List(async () => await ShowMenuAsync(),
                   ($"Update {tTypeName.ToLower()} name",
                   async () => team.Name = await _inputFormatter.InputText("Enter updating team`s name: ")
                ),
                   ("Quit", async () => { shouldUpdate = false; await Task.CompletedTask; }
                ));
            }

            if (shouldUpdate)
                await UpdateEntity(entity);
        }

        private async Task UpdateEntity(T entity)
        {
            try
            {
                await _entityService.Update(entity);
                _outputFormatter.Info($"The {tTypeName.ToLower()} was successfuly updated");
                _outputFormatter.Info(OutputMessageHalper.EntityInfo<T>(entity));
            }
            catch (Exception)
            {
                _outputFormatter.Info($"Error duering {tTypeName} updating");
            }
            finally
            {
                _outputFormatter.Quitable();
            }
        }
    }
}
