﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectsManagementTool.ConsoleApp.Interfaces
{
    public interface IHttpClientService
    {
        Task<T> GetAsync<T>(string requestUri);
        Task<List<T>> GetListAsync<T>(string requestUri);
        Task<T> PostAsync<T>(string requestUri, T obj);
        Task PutAsync<T>(string requestUri, T obj);
        Task DeleteAsync(string requestUri, int id);
    }
}
