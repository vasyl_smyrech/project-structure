﻿using System.Threading.Tasks;

namespace ProjectsManagementTool.ConsoleApp.Interfaces
{
    public interface IMarkTaskService
    {
        Task<int> MarkRandomTaskWithDelay(int delayInMs);
    }
}
