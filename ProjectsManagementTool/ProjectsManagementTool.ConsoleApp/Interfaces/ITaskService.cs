﻿using ProjectsManagementTool.Common.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectsManagementTool.ConsoleApp.Interfaces
{
    public interface ITaskService : IEntityService<TaskDto>
    {
        Task<List<TaskDto>> GetAllNotDoneTasks();
        Task MarkTaskAsDoneAsync(int shouldMarkAsDoneTaskId);
    }
}
