﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectsManagementTool.ConsoleApp.Interfaces
{
    public interface IEntityService<T>
    {
        Task<T> Create(T entity);
        Task<T> Read(int id);
        Task<IList<T>> ReadAll();
        Task Update(T user);
        Task Delete(int id);
    }
}
