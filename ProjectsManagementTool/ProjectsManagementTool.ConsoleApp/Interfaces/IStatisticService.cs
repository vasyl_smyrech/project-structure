﻿using ProjectsManagementTool.Common.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectsManagementTool.ConsoleApp.Interfaces
{
    public interface IAnaliticService
    {
        Task<ProjectTasksCountByProjectDto> GetProjectTasksCountByAuthorId(int authorId);
        Task<List<TaskDto>> GetPerformerTasksByNameLength(int performerId);
        Task<List<ShortTaskInfoDto>> GetPerformerFinishedTasksByYear(int performerId);
        Task<List<TeamWithMembersDto>> GetTeamsAndMembersFilteredByMembersAge();
        Task<List<UserWithTasksDto>> GetUsersByFirstNameWithTasksSortedByNameLength();
        Task<UserAnaliticInfoDto> GetUserAnaliticInfo(int userId);
        Task<List<ProjectAnaliticInfoDto>> GetProjectsAnaliticInfo();
    }
}
