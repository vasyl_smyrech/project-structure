﻿using System.Threading.Tasks;

namespace ProjectsManagementTool.ConsoleApp.Interfaces
{
    public interface IProjectManagementConsole
    {
        Task Start();
    }
}
