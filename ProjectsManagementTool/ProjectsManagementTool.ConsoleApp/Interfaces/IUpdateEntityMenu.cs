﻿using System.Threading.Tasks;

namespace ProjectsManagementTool.ConsoleApp.Interfaces
{
    public interface IUpdateEntityMenu<T>
    {
        Task ShowMenuAsync();
    }
}
