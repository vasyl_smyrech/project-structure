﻿using System;
using System.Drawing;
using System.Threading.Tasks;

namespace ProjectsManagementTool.ConsoleApp.Interfaces
{
    public interface IOutputFormatter
    {
        void ClearConsole();
        void CenteredText(string text);
        void CenteredText(string text, Color color);
        void Error(string errorMessage);
        void Error(string errorMessage, Action fallback);
        void Info(string info);
        void ListItem(string itemText);
        Task List(Func<Task> fallback, params (string, Func<Task>)[] items);
        void Quitable();
    }
}
