﻿using System;
using System.Threading.Tasks;

namespace ProjectsManagementTool.ConsoleApp.Interfaces
{
    public interface IAnaliticMenu
    {
        Task ShowMenuAsync(Func<Task> callback);
    }
}
