﻿using System.Threading.Tasks;

namespace ProjectsManagementTool.ConsoleApp.Interfaces
{
    public interface ICreateEntityDialog<T>
    {
        Task ShowDialogAsync();
    }
}
