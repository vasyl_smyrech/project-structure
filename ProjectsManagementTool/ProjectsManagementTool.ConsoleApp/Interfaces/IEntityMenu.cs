﻿using System;
using System.Threading.Tasks;

namespace ProjectsManagementTool.ConsoleApp.Interfaces
{
    public interface IEntityMenu<T>
    {
        Task ShowMenuAsync(Func<Task> callback);
    }
}
