﻿using System.Threading.Tasks;

namespace ProjectsManagementTool.ConsoleApp.Interfaces
{
    public interface IInputFormatter
    {
        Task<int?> InputPositiveInteger(string beforeInputText);
        Task<string> InputText(string beforeInputText);
    }
}
