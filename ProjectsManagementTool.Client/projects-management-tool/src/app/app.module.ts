import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { AppComponent, AppRoutingModule, CoreModule, TasksModule, 
  SharedModule, TeamsModule, ProjectsModule, UsersModule } from './index'

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    CoreModule,
    SharedModule,
    TasksModule,
    ProjectsModule,
    TeamsModule,
    UsersModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
