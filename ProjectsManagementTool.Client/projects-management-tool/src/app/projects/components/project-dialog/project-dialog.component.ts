import { Component, Inject, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Project, Team, User, ProjectDetails } from 'src/app/core';

@Component({
  selector: 'app-project-dialog',
  templateUrl: './project-dialog.component.html',
  styleUrls: ['./project-dialog.component.sass']
})
export class ProjectDialogComponent implements OnDestroy {
  public nameMaxLength = 100;
  public descriptionMaxLength = 300;

  public title: string;
  public project: Project;
  public teams: Team[];
  public authors: User[];
  public positiveButtonText: string;
  public negativeButtonText: string;
  public textPattern = new RegExp(".*[^ |\n|\t].*");

  private unsubscribe$ = new Subject<void>();

  constructor(
    private dialogRef: MatDialogRef<ProjectDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ProjectDetails,
  ) {
    this.project = data.project;
    this.teams = data.teams;
    this.authors = data.authors;
    this.negativeButtonText = data.negativeButtonText ?? 'Cancel';
    this.positiveButtonText = data.positiveButtonText ??
      this.project.id ? 'Edit' : 'Create'; 

    const projectIdInfo = this.project.id ? ` with id ${this.project.id}` : '';
    this.title = `${this.project.id ? 'Edit' : 'Create'} project${projectIdInfo}`;
  }

  public ngOnDestroy() {
      this.unsubscribe$.next();
      this.unsubscribe$.complete();
  }

  public close() {
      this.dialogRef.close();
  }

  public onConfirm() {
    this.dialogRef.close(this.project);
  }
}
  
  