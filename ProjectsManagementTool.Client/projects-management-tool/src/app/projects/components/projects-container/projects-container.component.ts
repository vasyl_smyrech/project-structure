import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { takeUntil } from 'rxjs/operators';
import { Subject, Observable } from 'rxjs';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

import { Project, ComponentCanDeactivate } from '../../../core/models';
import { ProjectService } from '../../../core';
import { ConfirmationDialogService, SnackBarService} from 'src/app/shared';
import { ProjectDialogService } from '../../services/project-dialog.service';

@Component({
  selector: 'app-projects-container',
  templateUrl: './projects-container.component.html',
  styleUrls: ['./projects-container.component.sass']
})
export class ProjectsContainerComponent implements OnInit, OnDestroy, ComponentCanDeactivate  {
  displayedColumns: string[] = ['id', 'name', 'description', 'created-at', 'deadline', 'delete-action', 'edit-action'];
  dataSource: MatTableDataSource<Project>;
  public projects: Project[] = [];
  public editedProject: Project;
  public loading = false;
  public loadingProjects = false;
  public showEntityContainer = false;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;


  private unsubscribe$ = new Subject<void>();

  constructor(
    private projectService: ProjectService,
    private projectDialogService: ProjectDialogService,
    private confirmationDialogService: ConfirmationDialogService,
    private snackBarService: SnackBarService,
  ) { }

  public canDeactivate(): boolean | Observable<boolean> {
    const tt = !this.editedProject || this.dataSource.data.includes(this.editedProject) 
    ? true 
    : confirm("You have unsaved changes. Changes won't be saved. Proceed?");
    return tt;
  }

  ngOnInit(): void {
    this.getProjects();
  }

  public getProjects() {
    this.loadingProjects = true;
    this.projectService
        .getProjects()
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(
          (resp) => {
            this.loadingProjects = false;
            this.projects = resp.body;
            this.dataSource = new MatTableDataSource(resp.body);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
          },
          () => this.loadingProjects = false
        );
  }

  public applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  public createProject() {
    this.projectDialogService
    .openCreationDialog()
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe(
      resp => {
        if (resp) {
          this.projectService
          .createProject(resp)
          .pipe(takeUntil(this.unsubscribe$))
          .subscribe(
            (result) => {
              this.dataSource.data.unshift(result.body);
              this.dataSource._updateChangeSubscription();
            },
            () => {
              this.snackBarService.showErrorMessage('Failed to create the project');
            }
          );}},
      (error) => {
        this.snackBarService.showErrorMessage(error);
      }
    );
  }

  public editProject(project: Project){
    this.editedProject = Object.assign({}, project);

    this.projectDialogService
    .openEditingDialog(project)
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe(
      resp => {
        if (resp) {
          this.projectService
          .updateProject(resp)
          .pipe(takeUntil(this.unsubscribe$))
          .subscribe(
            () => {},
            () => {
              this.revertChanges(resp.id);
              this.snackBarService.showErrorMessage('Failed to update the project');
            }
          );
        } else {
          this.revertChanges(project.id);
        }
      },
      (error) => {
        this.snackBarService.showErrorMessage(error);
        this.revertChanges(project.id);
      }
    );
  }

  public deleteProject(id: number){
    this.confirmationDialogService
    .openConfirmationDialog("Delete Project", "The project will be removed permanently!")
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe(
        resp => {
            if (resp) {
              this.projectService
              .deleteProject(id)
              .pipe(takeUntil(this.unsubscribe$))
              .subscribe(
                () => {
                  const index = this.dataSource.data.findIndex(p => p.id == id);
                  this.dataSource.data.splice(index, 1);
                  this.dataSource._updateChangeSubscription();
                },
                () => {
                  this.snackBarService.showErrorMessage('Failed to delete the project')
                }
              );
            }
        },
        error => this.snackBarService.showErrorMessage(error)
    );
}

  public ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  private revertChanges(projectId: number): void {
    const index = this.dataSource.data.findIndex(p => p.id == projectId);
    const exchangeProject =  Object.assign({}, this.editedProject);
    this.dataSource.data[index] = this.editedProject;
    this.editedProject = exchangeProject;
    this.dataSource._updateChangeSubscription();
  }
}
