import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProjectsContainerComponent } from './components/projects-container/projects-container.component';
import { UnsavedChangesGuard } from '../shared/guards/unsaved-changes.guard'

const routes: Routes = [
  {
    path: '',
    component: ProjectsContainerComponent,
    canDeactivate: [UnsavedChangesGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectsRoutingModule { }
