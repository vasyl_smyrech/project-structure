import { Injectable, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subject, combineLatest, Observable } from 'rxjs';
import { Project, UserService, TeamService } from '../../core'
import { ProjectDialogComponent } from '../components/project-dialog/project-dialog.component';
import { mergeMap } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class ProjectDialogService implements OnDestroy {
    private unsubscribe$ = new Subject<void>();

    public constructor(
        private dialog: MatDialog,
        private userService: UserService,
        private teamService: TeamService
        ) {}

    public openCreationDialog(): Observable<Project> {
       return this.openProjectDialog({name: '', description: '', deadline: new Date(Date.now())});
    }

    public openEditingDialog(project: Project): Observable<Project> {
        return this.openProjectDialog(project);
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    private openProjectDialog(project: Project): Observable<Project>{ 
        const authorsResponse$ = this.userService.getUsers();
        const teamsResponse$ = this.teamService.getTeams();

        return combineLatest(authorsResponse$, teamsResponse$)
        .pipe(mergeMap((res) => {
            const dialog = this.dialog.open(ProjectDialogComponent, {
                data: {
                  project: project,
                  authors: res[0].body,
                  teams: res[1].body
                },
                minWidth: 400,
                autoFocus: true,
                backdropClass: 'dialog-backdrop'
            });
    
            return dialog.afterClosed();   
        }));
    }
}
