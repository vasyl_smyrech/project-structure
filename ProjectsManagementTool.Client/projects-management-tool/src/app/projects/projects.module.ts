import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ProjectsRoutingModule } from './projects-routing.module';
import { ProjectsContainerComponent } from './components/projects-container/projects-container.component';
import { SharedModule } from '../shared';
import { ProjectDialogComponent } from './components/project-dialog/project-dialog.component';
import { ProjectDialogService } from './services/project-dialog.service'
import { UnsavedChangesGuard } from '../shared/guards/unsaved-changes.guard';



@NgModule({
  declarations: [ProjectsContainerComponent, ProjectDialogComponent],
  imports: [
    CommonModule,
    FormsModule,
    ProjectsRoutingModule,
    SharedModule
  ],
  providers:[
    UnsavedChangesGuard,
    ProjectDialogService
  ]
})
export class ProjectsModule { 
}
