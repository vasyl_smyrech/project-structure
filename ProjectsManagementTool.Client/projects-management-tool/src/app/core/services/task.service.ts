import { Injectable } from '@angular/core';

import { ProjectTask } from '../models';
import { HttpService } from '../services/http.service'

@Injectable({ providedIn: 'root' })
export class TaskService {
    public routePrefix = '/api/tasks';

    constructor(private httpService: HttpService) {}

    public getTasks() {
        return this.httpService.getFullRequest<ProjectTask[]>(`${this.routePrefix}`);
    }

    public createTask(task: ProjectTask) {
        return this.httpService.postFullRequest<ProjectTask>(`${this.routePrefix}`, task);
    }

    public updateTask(updatedTask: ProjectTask){
        return this.httpService.putRequest<ProjectTask>(`${this.routePrefix}`, updatedTask);
    }

    public deleteTask(taskId: number) {
        return this.httpService.deleteRequest<ProjectTask>(`${this.routePrefix}/${taskId}`)
    }
}
