import { Injectable } from '@angular/core';

import { User } from '../models';
import { HttpService } from '../services/http.service'

@Injectable({ providedIn: 'root' })
export class UserService {
    public routePrefix = '/api/users';

    constructor(private httpService: HttpService) {}

    public getUsers() {
        return this.httpService.getFullRequest<User[]>(`${this.routePrefix}`);
    }

    public createUser(user: User) {
        return this.httpService.postFullRequest<User>(`${this.routePrefix}`, user);
    }

    public updateUser(updatedUser: User){
        return this.httpService.putRequest<User>(`${this.routePrefix}`, updatedUser);
    }

    public deleteUser(userId: number) {
        return this.httpService.deleteRequest<User>(`${this.routePrefix}/${userId}`)
    }
}
