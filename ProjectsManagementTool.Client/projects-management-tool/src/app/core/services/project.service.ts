import { Injectable } from '@angular/core';

import { Project } from '../models';
import { HttpService } from '../services/http.service'
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class ProjectService {
    public routePrefix = '/api/projects';

    constructor(private httpService: HttpService) {}

    public getProjects() {
        return this.httpService.getFullRequest<Project[]>(`${this.routePrefix}`);
    }

    public getProjectById(id: number) {
        return this.httpService.getFullRequest<Project>(`${this.routePrefix}/${id}`);
    }

    public createProject(project: Project) {
        return this.httpService.postFullRequest<Project>(`${this.routePrefix}`, project);
    }

    public updateProject(updatedProject: Project){
        return this.httpService.putRequest<Project>(`${this.routePrefix}`, updatedProject);
    }

    public deleteProject(projectId: number) {
        return this.httpService.deleteRequest<Project>(`${this.routePrefix}/${projectId}`)
    }
}
