import { Injectable } from '@angular/core';

import { Team } from '../models';
import { HttpService } from '../services/http.service'

@Injectable({ providedIn: 'root' })
export class TeamService {
    public routePrefix = '/api/teams';

    constructor(private httpService: HttpService) {}

    public getTeams() {
        return this.httpService.getFullRequest<Team[]>(`${this.routePrefix}`);
    }

    public createTeam(team: Team) {
        return this.httpService.postFullRequest<Team>(`${this.routePrefix}`, team);
    }

    public updateTeam(updatedTeam: Team){
        return this.httpService.putRequest<Team>(`${this.routePrefix}`, updatedTeam);
    }

    public deleteTeam(teamId: number) {
        return this.httpService.deleteRequest<Team>(`${this.routePrefix}/${teamId}`)
    }
}
