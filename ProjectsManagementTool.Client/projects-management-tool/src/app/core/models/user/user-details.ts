import {Project, User, Team } from '..'

export interface UserDetails {
    user?: User;
    teams: Team[];
    positiveButtonText?: string;
    negativeButtonText?: string;
}