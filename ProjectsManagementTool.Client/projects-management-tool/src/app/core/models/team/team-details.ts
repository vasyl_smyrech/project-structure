import { Team } from "..";

export interface TeamDetails {
    team?: Team;
    positiveButtonText?: string;
    negativeButtonText?: string;
}