import { TaskState } from "..";

export interface ProjectTask {
    id?: number;
    name: string;
    description: string;
    createdAt?: Date;
    finishedAt?: Date;
    state: TaskState;
    projectId?: number;
    performerId?: number;
}