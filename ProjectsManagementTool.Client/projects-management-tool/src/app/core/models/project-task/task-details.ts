import { User, Team, ProjectTask, Project } from '..'

export interface TaskDetails {
    task?: ProjectTask;
    performers: User[];
    projects: Project[];
    positiveButtonText?: string;
    negativeButtonText?: string;
}