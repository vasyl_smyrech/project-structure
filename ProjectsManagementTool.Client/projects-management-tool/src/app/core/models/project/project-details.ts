import {Project, User, Team } from '..'

export interface ProjectDetails {
    project?: Project;
    authors: User[];
    teams: Team[];
    positiveButtonText?: string;
    negativeButtonText?: string;
}