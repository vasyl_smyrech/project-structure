import { Team, User, ProjectTask } from "..";

export interface Project {
    id?: number;
    name: string;
    description: string;
    createdAt?: Date;
    deadline?: Date;
    authorId?: number
    author?: User;
    teamId?: number;
    team?: Team;
    tasks?: ProjectTask []; 
}