import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { ErrorInterceptor } from './interceptors/error.interceptor';
import {
  HttpService,
  ProjectService,
  TaskService,
  TeamService,
  UserService
} from './services';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule
  ],
  providers:[
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    HttpService,
    ProjectService,
    TaskService,
    TeamService,
    UserService
  ]
})
export class CoreModule { }
