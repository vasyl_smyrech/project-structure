import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { ErrorCode } from '../models/error-code';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    constructor(private router: Router) {}

    public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(req).pipe(
            catchError((response) => {
                if (response.status === 401) {
                    if (response.error) {
                            return throwError(response.error.error);
                    }
                }

                console.log(response);
                const error = response.error && response.status !== 0
                    ? this.formatErrors(response.error) || response.error.message || `Response: ${response.status} - ${response.error}`
                    : response.message || `${response.status} ${response.statusText}`;

                return throwError(error);
            })
        );
    }

    private formatErrors(error: any) {
        if (!error || !error.errors) return null;
        let errors = error.errors as string[];
        return errors.join('\n');
        
    }
}
