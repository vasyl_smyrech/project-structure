import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TasksContainerComponent } from './components/tasks-container/tasks-container.component';

const routes: Routes = [
  {
    path: '',
    component: TasksContainerComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TasksRoutingModule { }
