import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TasksContainerComponent } from './components/tasks-container/tasks-container.component';
import { TasksRoutingModule } from './tasks-routing.module'
import { UnsavedChangesGuard } from '../shared/guards/unsaved-changes.guard';
import { SharedModule } from '../shared';
import { FormsModule } from '@angular/forms';
import { TaskDialogService } from './services/task-dialog.service';
import { TaskDialogComponent } from './components/task-dialog/task-dialog.component'



@NgModule({
  declarations: [
    TasksContainerComponent,
    TaskDialogComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    TasksRoutingModule,
    SharedModule
  ],
  providers:[
    UnsavedChangesGuard,
    TaskDialogService
  ]
})
export class TasksModule { }
