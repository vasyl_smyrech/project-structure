import { Component, Inject, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ProjectTask, User, Project, TaskState } from '../../../core';
import { TaskDetails } from '../../../core/models/project-task/task-details';

@Component({
  selector: 'app-task-dialog',
  templateUrl: './task-dialog.component.html',
  styleUrls: ['./task-dialog.component.sass']
})
export class TaskDialogComponent implements OnDestroy {
  public nameMaxLength = 100;
  public descriptionMaxLength = 300;

  public title: string;
  public task: ProjectTask;
  public performers: User[];
  public projects: Project[];
  public positiveButtonText: string;
  public negativeButtonText: string;
  public textPattern = new RegExp(".*[^ |\n|\t].*");
  public states = TaskState;
  public keys = Object.keys;

  private unsubscribe$ = new Subject<void>();

  constructor(
    private dialogRef: MatDialogRef<TaskDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: TaskDetails,
  ) {
    this.task = data.task;
    this.performers = data.performers;
    this.projects = data.projects;
    this.negativeButtonText = data.negativeButtonText ?? 'Cancel';
    this.positiveButtonText = data.positiveButtonText ??
      this.task.id ? 'Edit' : 'Create'; 

    const taskInfo = this.task.id ? ` with id ${this.task.id}` : '';
    this.title = `${this.task.id ? 'Edit' : 'Create'} task${taskInfo}`;
  }

  public ngOnDestroy() {
      this.unsubscribe$.next();
      this.unsubscribe$.complete();
  }

  public close() {
      this.dialogRef.close();
  }

  public onConfirm() {
    this.dialogRef.close(this.task);
  }
}
  
  