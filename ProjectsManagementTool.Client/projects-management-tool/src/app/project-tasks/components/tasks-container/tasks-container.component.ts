import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { takeUntil } from 'rxjs/operators';
import { Subject, Observable } from 'rxjs';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

import { ComponentCanDeactivate, ProjectTask, TaskState } from '../../../core/models';
import { TaskService } from '../../../core';
import { ConfirmationDialogService, SnackBarService} from 'src/app/shared';
import { TaskDialogService } from '../../services/task-dialog.service';

@Component({
  selector: 'app-tasks-container',
  templateUrl: './tasks-container.component.html',
  styleUrls: ['./tasks-container.component.sass']
})
export class TasksContainerComponent implements OnInit, OnDestroy, ComponentCanDeactivate  {
  displayedColumns: string[] = ['id', 'name', 'description', 'created-at', 'delete-action', 'edit-action'];
  dataSource: MatTableDataSource<ProjectTask>;
  public tasks: ProjectTask[] = [];
  public editedTask: ProjectTask;
  public loading = false;
  public loadingTasks = false;
  public showEntityContainer = false;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;


  private unsubscribe$ = new Subject<void>();

  constructor(
    private taskService: TaskService,
    private taskDialogService: TaskDialogService,
    private confirmationDialogService: ConfirmationDialogService,
    private snackBarService: SnackBarService,
  ) { }

  public canDeactivate(): boolean | Observable<boolean> {
    return !this.editedTask || this.dataSource.data.includes(this.editedTask) 
    ? true 
    : confirm("You have unsaved changes. Changes won't be saved. Proceed?");
  }

  ngOnInit(): void {
    this.getTasks();
  }

  public getTasks() {
    this.loadingTasks = true;
    this.taskService
        .getTasks()
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(
          (resp) => {
            this.loadingTasks = false;
            this.tasks = resp.body;
            this.dataSource = new MatTableDataSource(resp.body);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
          },
          () => this.loadingTasks = false
        );
  }

  public applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  public createTask() {
    this.taskDialogService
    .openCreationDialog()
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe(
      resp => {
        if (resp) {
          this.taskService
          .createTask(resp)
          .pipe(takeUntil(this.unsubscribe$))
          .subscribe(
            (result) => {
              this.dataSource.data.unshift(result.body);
              this.dataSource._updateChangeSubscription();
            },
            () => {
              this.snackBarService.showErrorMessage('Failed to create the project');
            }
          );}},
      (error) => {
        this.snackBarService.showErrorMessage(error);
      }
    );
  }

  public editTask(task: ProjectTask){
    this.editedTask = Object.assign({}, task);

    this.taskDialogService
    .openEditingDialog(task)
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe(
      resp => {
        if (resp) {
          this.taskService
          .updateTask(resp)
          .pipe(takeUntil(this.unsubscribe$))
          .subscribe(
            () => {},
            () => {
              this.revertChanges(resp.id);
              this.snackBarService.showErrorMessage('Failed to update the task');
            }
          );
        } else {
          this.revertChanges(task.id);
        }
      },
      (error) => {
        this.snackBarService.showErrorMessage(error);
        this.revertChanges(task.id);
      }
    );
  }

  public deleteTask(id: number){
    this.confirmationDialogService
    .openConfirmationDialog("Delete task", "The task will be removed permanently!")
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe(
        resp => {
            if (resp) {
              this.taskService
              .deleteTask(id)
              .pipe(takeUntil(this.unsubscribe$))
              .subscribe(
                () => {
                  const index = this.dataSource.data.findIndex(p => p.id == id);
                  this.dataSource.data.splice(index, 1);
                  this.dataSource._updateChangeSubscription();
                },
                () => {
                  this.snackBarService.showErrorMessage('Failed to delete the task')
                }
              );
            }
        },
        error => this.snackBarService.showErrorMessage(error)
    );
  }

  public getState(state: TaskState): string {
    switch (state) {
      case TaskState.ToDo: 
        return 'To do';
      case TaskState.InProgress: 
        return 'In progress';
      case TaskState.Done: 
        return 'Done';
      case TaskState.Canceled: 
        return 'Canceled';
    }
  }

  public ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  private revertChanges(taskId: number): void {
    const index = this.dataSource.data.findIndex(p => p.id == taskId);
    const exchangeTask =  Object.assign({}, this.editedTask);
    this.dataSource.data[index] = this.editedTask;
    this.editedTask = exchangeTask;
    this.dataSource._updateChangeSubscription();
  }
}
