import { Injectable, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subject, Observable, combineLatest } from 'rxjs';
import { UserService, ProjectTask, TaskState, ProjectService } from '../../core'
import { mergeMap } from 'rxjs/operators';
import { TaskDialogComponent } from '../components/task-dialog/task-dialog.component';

@Injectable({ providedIn: 'root' })
export class TaskDialogService implements OnDestroy {
    private unsubscribe$ = new Subject<void>();

    public constructor(
        private dialog: MatDialog,
        private userService: UserService,
        private projectService: ProjectService
        ) {}

    public openCreationDialog(): Observable<ProjectTask> {
       return this.openTaskDialog({name: '', description: '', state: TaskState.ToDo});
    }

    public openEditingDialog(project: ProjectTask): Observable<ProjectTask> {
        return this.openTaskDialog(project);
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    private openTaskDialog(task: ProjectTask): Observable<ProjectTask>{ 
        const performersResponse$ = this.userService.getUsers();
        const projectsResponse$ = this.projectService.getProjects();

        return combineLatest(performersResponse$, projectsResponse$)
        .pipe(mergeMap((res) => {
            const dialog = this.dialog.open(TaskDialogComponent, {
                data: {
                  task: task,
                  performers: res[0].body,
                  projects: res[1].body
                },
                minWidth: 400,
                autoFocus: true,
                backdropClass: 'dialog-backdrop'
            });
    
            return dialog.afterClosed();   
        }));
    }
}
