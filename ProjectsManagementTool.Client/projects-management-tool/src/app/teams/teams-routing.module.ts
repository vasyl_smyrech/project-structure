import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TeamsContainerComponent } from './components/teams-container/teams-container.component';
import { UnsavedChangesGuard } from '../shared/guards/unsaved-changes.guard'

const routes: Routes = [
  {
    path: '',
    component: TeamsContainerComponent,
    canDeactivate: [UnsavedChangesGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TeamsRoutingModule { }
