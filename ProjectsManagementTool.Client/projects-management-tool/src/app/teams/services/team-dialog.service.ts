import { Injectable, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subject, Observable } from 'rxjs';
import { Team } from '../../core'
import { TeamDialogComponent } from '../components/team-dialog/team-dialog.component';

@Injectable({ providedIn: 'root' })
export class TeamDialogService implements OnDestroy {
    private unsubscribe$ = new Subject<void>();

    public constructor(
        private dialog: MatDialog,
        ) {}

    public openCreationDialog(): Observable<Team> {
       return this.openTeamDialog({name: ''});
    }

    public openEditingDialog(team: Team): Observable<Team> {
        return this.openTeamDialog(team);
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    private openTeamDialog(team: Team): Observable<Team>{ 
        const dialog = this.dialog.open(TeamDialogComponent, {
            data: {
                team: team,
            },
            minWidth: 400,
            autoFocus: true,
            backdropClass: 'dialog-backdrop'
        });

        return dialog.afterClosed();   
    }
}
