import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { takeUntil } from 'rxjs/operators';
import { Subject, Observable } from 'rxjs';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

import { ComponentCanDeactivate, Team, TeamService } from '../../../core';
import { ConfirmationDialogService, SnackBarService } from '../../../shared';
import { TeamDialogService } from '../../services/team-dialog.service';

@Component({
  selector: 'app-teams-container',
  templateUrl: './teams-container.component.html',
  styleUrls: ['./teams-container.component.sass']
})
export class TeamsContainerComponent implements OnInit, OnDestroy, ComponentCanDeactivate  {
  displayedColumns: string[] = ['id', 'name', 'created-at', 'delete-action', 'edit-action'];
  dataSource: MatTableDataSource<Team>;
  public teams: Team[] = [];
  public editedTeam: Team;
  public loading = false;
  public loadingTeams = false;
  public showEntityContainer = false;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  private unsubscribe$ = new Subject<void>();

  constructor(
    private teamService: TeamService,
    private teamDialogService: TeamDialogService,
    private confirmationDialogService: ConfirmationDialogService,
    private snackBarService: SnackBarService,
  ) { }

  public canDeactivate(): boolean | Observable<boolean> {
    return !this.editedTeam || this.dataSource.data.includes(this.editedTeam) 
    ? true 
    : confirm("You have unsaved changes. Changes won't be saved. Proceed?");
  }

  ngOnInit(): void {
    this.getTeams();
  }

  public getTeams() {
    this.loadingTeams = true;
    this.teamService
        .getTeams()
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(
          (resp) => {
            this.loadingTeams = false;
            this.teams = resp.body;
            this.dataSource = new MatTableDataSource(resp.body);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
          },
          () => this.loadingTeams = false
        );
  }

  public applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  public createTeam() {
    this.teamDialogService
    .openCreationDialog()
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe(
      resp => {
        if (resp) {
          this.teamService
          .createTeam(resp)
          .pipe(takeUntil(this.unsubscribe$))
          .subscribe(
            (result) => {
              this.dataSource.data.unshift(result.body);
              this.dataSource._updateChangeSubscription();
            },
            () => {
              this.snackBarService.showErrorMessage('Failed to create the team');
            }
          );}},
      (error) => {
        this.snackBarService.showErrorMessage(error);
      }
    );
  }

  public editTeam(team: Team){
    this.editedTeam = Object.assign({}, team);

    this.teamDialogService
    .openEditingDialog(team)
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe(
      resp => {
        if (resp) {
          this.teamService
          .updateTeam(resp)
          .pipe(takeUntil(this.unsubscribe$))
          .subscribe(
            () => {},
            () => {
              this.revertChanges(resp.id);
              this.snackBarService.showErrorMessage('Failed to update the team');
            }
          );
        } else {
          this.revertChanges(team.id);
        }
      },
      (error) => {
        this.snackBarService.showErrorMessage(error);
        this.revertChanges(team.id);
      }
    );
  }

  public deleteTeam(id: number){
    this.confirmationDialogService
    .openConfirmationDialog("Delete team", "The team will be removed permanently!")
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe(
        resp => {
            if (resp) {
              this.teamService
              .deleteTeam(id)
              .pipe(takeUntil(this.unsubscribe$))
              .subscribe(
                () => {
                  const index = this.dataSource.data.findIndex(p => p.id == id);
                  this.dataSource.data.splice(index, 1);
                  this.dataSource._updateChangeSubscription();
                },
                () => {
                  this.snackBarService.showErrorMessage('Failed to delete the team')
                }
              );
            }
        },
        error => this.snackBarService.showErrorMessage(error)
    );
  }

  public ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  private revertChanges(teamId: number): void {
    const index = this.dataSource.data.findIndex(p => p.id == teamId);
    const exchangeTeam =  Object.assign({}, this.editedTeam);
    this.dataSource.data[index] = this.editedTeam;
    this.editedTeam = exchangeTeam;
    this.dataSource._updateChangeSubscription();
  }
}
