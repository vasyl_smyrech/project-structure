import { Component, Inject, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { Team, TeamDetails } from '../../../core';

@Component({
  selector: 'app-team-dialog',
  templateUrl: './team-dialog.component.html',
  styleUrls: ['./team-dialog.component.sass']
})
export class TeamDialogComponent implements OnDestroy {
  public nameMaxLength = 100;
  public descriptionMaxLength = 300;

  public title: string;
  public team: Team;
  public positiveButtonText: string;
  public negativeButtonText: string;
  public textPattern = new RegExp(".*[^ |\n|\t].*");

  private unsubscribe$ = new Subject<void>();

  constructor(
    private dialogRef: MatDialogRef<TeamDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: TeamDetails,
  ) {
    this.team = data.team;
    this.negativeButtonText = data.negativeButtonText ?? 'Cancel';
    this.positiveButtonText = data.positiveButtonText ??
      this.team.id ? 'Edit' : 'Create'; 

    const teamIdInfo = this.team.id ? ` with id ${this.team.id}` : '';
    this.title = `${this.team.id ? 'Edit' : 'Create'} team${teamIdInfo}`;
  }

  public ngOnDestroy() {
      this.unsubscribe$.next();
      this.unsubscribe$.complete();
  }

  public close() {
      this.dialogRef.close();
  }

  public onConfirm() {
    this.dialogRef.close(this.team);
  }
}
  
  