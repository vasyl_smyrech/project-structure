import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { TeamsRoutingModule } from './teams-routing.module';
import { SharedModule } from '../shared';
import { TeamsContainerComponent } from './components/teams-container/teams-container.component'
import { TeamDialogComponent } from './components/team-dialog/team-dialog.component';
import { TeamDialogService } from './services/team-dialog.service'
import { UnsavedChangesGuard } from '../shared/guards/unsaved-changes.guard';



@NgModule({
  declarations: [TeamsContainerComponent, TeamDialogComponent],
  imports: [
    CommonModule,
    FormsModule,
    TeamsRoutingModule,
    SharedModule
  ],
  providers:[
    UnsavedChangesGuard,
    TeamDialogService
  ]
})
export class TeamsModule { 
}
