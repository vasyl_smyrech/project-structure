import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { UsersContainerComponent } from './components/users-container/users-container.component';
import { UsersRoutingModule } from './users-routing.module'
import { SharedModule } from '../shared';
import { UnsavedChangesGuard } from '../shared/guards/unsaved-changes.guard';
import { UserDialogComponent } from './components/user-dialog/user-dialog.component';
import { UserDialogService } from './services/user-dialog.service';




@NgModule({
  declarations: [UsersContainerComponent, UserDialogComponent],
  imports: [
    CommonModule,
    UsersRoutingModule,
    FormsModule,
    UsersRoutingModule,
    SharedModule
  ],
  providers:[
    UnsavedChangesGuard,
    UserDialogService
  ]
})
export class UsersModule { }

