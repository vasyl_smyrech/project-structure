import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsersContainerComponent } from './components/users-container/users-container.component';
import { UnsavedChangesGuard } from '../shared/guards/unsaved-changes.guard';

const routes: Routes = [
  {
    path: '',
    component: UsersContainerComponent,
    canDeactivate: [UnsavedChangesGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }