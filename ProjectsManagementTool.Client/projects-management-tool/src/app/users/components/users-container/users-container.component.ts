import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { takeUntil } from 'rxjs/operators';
import { Subject, Observable } from 'rxjs';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

import { ComponentCanDeactivate, User, UserService } from '../../../core';
import { ConfirmationDialogService, SnackBarService} from 'src/app/shared';
import { UserDialogService } from '../../services/user-dialog.service';

@Component({
  selector: 'app-users-container',
  templateUrl: './users-container.component.html',
  styleUrls: ['./users-container.component.sass']
})
export class UsersContainerComponent implements OnInit, OnDestroy, ComponentCanDeactivate  {
  displayedColumns: string[] = ['id', 'first-name', 'last-name', 'birthday', 'registered-at', 'delete-action', 'edit-action'];
  dataSource: MatTableDataSource<User>;
  public users: User[] = [];
  public editedUser: User;
  public loading = false;
  public loadingUsers = false;
  public showEntityContainer = false;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;


  private unsubscribe$ = new Subject<void>();

  constructor(
    private userService: UserService,
    private userDialogService: UserDialogService,
    private confirmationDialogService: ConfirmationDialogService,
    private snackBarService: SnackBarService,
  ) { }

  public canDeactivate(): boolean | Observable<boolean> {
    return !this.editedUser || this.dataSource.data.includes(this.editedUser) 
      ? true 
      : confirm("You have unsaved changes. Changes won't be saved. Proceed?");
  }

  ngOnInit(): void {
    this.getUsers();
  }

  public getUsers() {
    this.loadingUsers = true;
    this.userService
        .getUsers()
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(
          (resp) => {
            this.loadingUsers = false;
            this.users = resp.body;
            this.dataSource = new MatTableDataSource(resp.body);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
          },
          () => this.loadingUsers = false
        );
  }

  public applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  public createUser() {
    this.userDialogService
    .openCreationDialog()
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe(
      resp => {
        if (resp) {
          this.userService
          .createUser(resp)
          .pipe(takeUntil(this.unsubscribe$))
          .subscribe(
            (result) => {
              this.dataSource.data.unshift(result.body);
              this.dataSource._updateChangeSubscription();
            },
            () => {
              this.snackBarService.showErrorMessage('Failed to create the user');
            }
          );}},
      (error) => {
        this.snackBarService.showErrorMessage(error);
      }
    );
  }

  public editUser(user: User){
    this.editedUser = Object.assign({}, user);

    this.userDialogService
    .openEditingDialog(user)
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe(
      resp => {
        if (resp) {
          this.userService
          .updateUser(resp)
          .pipe(takeUntil(this.unsubscribe$))
          .subscribe(
            () => {},
            () => {
              this.revertChanges(resp.id);
              this.snackBarService.showErrorMessage('Failed to update the user');
            }
          );
        } else {
          this.revertChanges(user.id);
        }
      },
      (error) => {
        this.snackBarService.showErrorMessage(error);
        this.revertChanges(user.id);
      }
    );
  }

  public deleteUser(id: number){
    this.confirmationDialogService
    .openConfirmationDialog("Delete user", "The user will be removed permanently!")
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe(
        resp => {
            if (resp) {
              this.userService
              .deleteUser(id)
              .pipe(takeUntil(this.unsubscribe$))
              .subscribe(
                () => {
                  const index = this.dataSource.data.findIndex(p => p.id == id);
                  this.dataSource.data.splice(index, 1);
                  this.dataSource._updateChangeSubscription();
                },
                () => {
                  this.snackBarService.showErrorMessage('Failed to delete the u')
                }
              );
            }
        },
        error => this.snackBarService.showErrorMessage(error)
    );
}

  public ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  private revertChanges(userId: number): void {
    const index = this.dataSource.data.findIndex(p => p.id == userId);
    const exchangeUser =  Object.assign({}, this.editedUser);
    this.dataSource.data[index] = this.editedUser;
    this.editedUser = exchangeUser;
    this.dataSource._updateChangeSubscription();
  }
}
