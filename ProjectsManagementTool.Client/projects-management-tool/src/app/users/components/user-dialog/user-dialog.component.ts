import { Component, Inject, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { User, Team, UserDetails } from '../../../core';

@Component({
  selector: 'app-user-dialog',
  templateUrl: './user-dialog.component.html',
  styleUrls: ['./user-dialog.component.sass']
})
export class UserDialogComponent implements OnDestroy {
  public nameMaxLength = 100;
  public descriptionMaxLength = 300;

  public title: string;
  public user: User;
  public teams: Team[];
  public positiveButtonText: string;
  public negativeButtonText: string;
  public textPattern = new RegExp(".*[^ |\n|\t].*");
  public usernamePattern = new RegExp("^[a-zA-Z0-9 ]+$");

  private unsubscribe$ = new Subject<void>();

  constructor(
    private dialogRef: MatDialogRef<UserDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: UserDetails,
  ) {
    this.user = data.user;
    this.teams = data.teams;
    this.negativeButtonText = data.negativeButtonText ?? 'Cancel';
    this.positiveButtonText = data.positiveButtonText ??
      this.user.id ? 'Edit' : 'Create'; 

    const userInfo = this.user.id ? ` with id ${this.user.id}` : '';
    this.title = `${this.user.id ? 'Edit' : 'Create'} task${userInfo}`;
  }

  public ngOnDestroy() {
      this.unsubscribe$.next();
      this.unsubscribe$.complete();
  }

  public close() {
      this.dialogRef.close();
  }

  public onConfirm() {
    this.dialogRef.close(this.user);
  }
}
  
  