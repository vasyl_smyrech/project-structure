import { Injectable, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subject, Observable } from 'rxjs';
import { TeamService, User } from '../../core';
import { mergeMap } from 'rxjs/operators';
import { UserDialogComponent } from '../components/user-dialog/user-dialog.component';

@Injectable({ providedIn: 'root' })
export class UserDialogService implements OnDestroy {
    private unsubscribe$ = new Subject<void>();

    public constructor(
        private dialog: MatDialog,
        private teamService: TeamService
        ) {}

    public openCreationDialog(): Observable<User> {
       return this.openUserDialog({firstName: '', lastName: '', email: ''});
    }

    public openEditingDialog(user: User): Observable<User> {
        return this.openUserDialog(user);
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    private openUserDialog(user: User): Observable<User>{ 
        return this.teamService.getTeams()
            .pipe(mergeMap((res) => {
                const dialog = this.dialog.open(UserDialogComponent, {
                    data: {
                    user: user,
                    teams: res.body
                    },
                    minWidth: 400,
                    autoFocus: true,
                    backdropClass: 'dialog-backdrop'
            });
    
            return dialog.afterClosed();   
        }));
    }
}
