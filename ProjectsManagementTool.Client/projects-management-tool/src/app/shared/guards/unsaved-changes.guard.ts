import { Injectable } from '@angular/core';
import { Router, CanDeactivate } from '@angular/router';
import { ComponentCanDeactivate } from '../../core/models';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class UnsavedChangesGuard implements CanDeactivate<ComponentCanDeactivate> {
    constructor(private router: Router) {}

    public canDeactivate(component: ComponentCanDeactivate): Observable<boolean> | boolean {       
        return component.canDeactivate ? component.canDeactivate() : true;
    }
}
