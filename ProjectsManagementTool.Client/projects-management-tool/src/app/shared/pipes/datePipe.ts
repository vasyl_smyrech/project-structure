import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'ukrDate'
})
export class UkrDatePipe implements PipeTransform {
    private months = [
        'cічня', 
        'лютого', 
        'березня', 
        'квітня', 
        'травня', 
        'червня', 
        'липня', 
        'серпня', 
        'вересня', 
        'жовтня', 
        'листопада',
        'грудня'];

    transform(value: Date): any {
        const date = new Date(value);

        return value ? 
            `${date.getDate()} ${this.months[date.getMonth()]} ${date.getFullYear()}`
            : '';
    }
}