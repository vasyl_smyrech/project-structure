import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UnsavedChangesGuard } from './guards/unsaved-changes.guard';

const routes: Routes = [
  {
    path: 'projects',
    loadChildren: '../projects/projects.module#ProjectsModule',
    pathMatch: 'full',
    //canDeactivate: [UnsavedChangesGuard]
  },
  {
    path: 'tasks',
    loadChildren: '../project-tasks/tasks.module#TasksModule',
    pathMatch: 'full'
  },
  {
    path: 'users',
    loadChildren: '../users/users.module#UsersModule',
    pathMatch: 'full'
  },
  {
    path: 'teams',
    loadChildren: '../teams/teams.module#TeamsModule',
    pathMatch: 'full'
  },
  {
    path: '**', redirectTo: 'projects'
  }
];


@NgModule({
  providers:[
    UnsavedChangesGuard
  ],
  imports:
  [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SharedRoutingModule { }
