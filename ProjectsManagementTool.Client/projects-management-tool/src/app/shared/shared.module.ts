import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { SharedRoutingModule } from './shared-routing.module';
import { ConfirmationDialogComponent } from './components/confirmation-dialog/confirmation-dialog.component';
import { MaterialComponentsModule } from './material-components/material-components.module';
import { UkrDatePipe } from './pipes/datePipe'
import { TaskStateColorDirective } from './directives/task-state-directive';


@NgModule({
  declarations: [
    HeaderComponent, 
    FooterComponent,
    UkrDatePipe,
    TaskStateColorDirective,
    ConfirmationDialogComponent
  ],
  imports: [
    CommonModule,
    MaterialComponentsModule,
    SharedRoutingModule
  ],
  exports: [
    HeaderComponent, 
    FooterComponent,
    MaterialComponentsModule,
    UkrDatePipe,
    TaskStateColorDirective,
    ConfirmationDialogComponent,
  ],
  // providers:[
  //   UnsavedChangesGuard
  // ]
})
export class SharedModule { }
