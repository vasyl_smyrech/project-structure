import { Directive, ElementRef, Input, OnInit, HostListener, SimpleChanges } from '@angular/core';
import { TaskState } from '../../core';

@Directive({
  selector: '[taskStateHighlight]'
})
export class TaskStateColorDirective implements OnInit {
    @Input() taskStateHighlight: TaskState;

  constructor(private el: ElementRef) { }

    ngOnInit(): void {
        this.el.nativeElement.style.backgroundColor = this.getBgColor();
    }

    ngOnChanges(changes: SimpleChanges){
        if(changes.taskStateHighlight){
            this.el.nativeElement.style.backgroundColor = this.getBgColor();
        }
      }

    private getBgColor(): string {
        switch(this.taskStateHighlight) {
            case TaskState.ToDo: 
                return 'yellow';
            case TaskState.Canceled:
                return 'orange';
            case TaskState.Done:
                return '#afb';
            case TaskState.InProgress:
                return '#abf';
            default: return 'white';
        }
    }
}